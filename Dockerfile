FROM node:14.18 AS asset-builder

# ensure up to date npm
RUN npm install -g npm@8.3.0

WORKDIR /assets
COPY src/main/webapp/package*.json ./
RUN npm ci --production

FROM maven:3.6.3-openjdk-11 AS build

WORKDIR /build
COPY pom.xml .
RUN mvn install

COPY --from=asset-builder \
    /assets/dist/ \
    /build/src/main/webapp/dist/

COPY src /build/src
RUN mvn package

# build artefact: /build/target/*.war

FROM tomcat:9.0.53-jre11-temurin-focal

LABEL \
    org.label-schema.dockerfile="/Dockerfile" \
    org.label-schema.license=" AGPL-3.0-or-later" \
    org.label-schema.maintainer="FE" \
    org.label-schema.name="ROGER" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://gitlab.gwdg.de/fe/roger" \
    org.label-schema.vcs-url="https://gitlab.gwdg.de/fe/roger" \
    org.label-schema.vendor="FE"

COPY --from=build /build/target/*.war /usr/local/tomcat/webapps

ARG build_date
ARG vcs_ref
ARG version
LABEL org.label-schema.version="$version" \
      org.label-schema.vcs-ref="$vcs_ref" \
      org.label-schema.build-date="$build_date"
