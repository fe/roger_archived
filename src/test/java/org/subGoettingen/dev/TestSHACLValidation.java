/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 * 
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.subGoettingen.dev;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 */
public class TestSHACLValidation {

  /**
   * 
   */
  @Test
  @Ignore
  public void testValidateAgainstSchema() {
    SHACL shacl = new SHACL();

    if (shacl.validateDataAgainstGraph(
        RDFDataMgr.loadModel("src/main/webapp/bdnForms.ttl").getGraph(),
        RDFDataMgr.loadGraph("src/main/webapp/dataexp.ttl"))) {
      System.out.println("Data is valid");
    } else {
      System.out.println("Data is NO valid. Check the data file");
    }
  }


  /*
   * @Test
   * 
   * @Ignore public void testInsertDataFromFile() throws IOException, ObjectNotFoundFault,
   * AuthFault, IoFault, MetadataParseFault, CrudClientException{
   * 
   * System.out.println(readFromCrud("textgrid:255k8.1")); saveToFuseki("textgrid:255k8.1"); }
   * 
   * public String readFromCrud(String uri) throws CrudClientException, IOException,
   * ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault{ CrudClient crudclient = null; try
   * { crudclient = new CrudClient("https://textgridlab.org/1.0/tgcrud/TGCrudService?wsdl")
   * .enableGzipCompression(); } catch (CrudClientException e) { // TODO Auto-generated catch block
   * e.printStackTrace(); } info.textgrid.clients.tgcrud.TextGridObject tgobj = crudclient.read()
   * .setTextgridUri(uri).execute();
   * 
   * saveObjectAsNewRevision(uri, tgobj);
   * 
   * return IOUtils.toString(tgobj.getData(), StandardCharsets.UTF_8); }
   * 
   * public void saveObjectAsNewRevision(String tgURI, info.textgrid.clients.tgcrud.TextGridObject
   * tgobj) throws ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault, IOException{
   * TGCrudService tgCRUD =
   * TGCrudClientUtilities.getTgcrud("https://dev.textgridlab.org/1.0/tgcrud/TGCrudService?wsdl");
   * //no sids in git -> Enter sid before running String sid ="";
   * 
   * MetadataContainerType metaContainer = tgobj.getMetadatada(); Holder<MetadataContainerType>
   * tgMeta = new Holder<MetadataContainerType>(); tgMeta.value = metaContainer;
   * 
   * DataSource dataSource = new ByteArrayDataSource(tgobj.getData(), "UTF-8"); DataHandler tgData =
   * new DataHandler(dataSource);
   * 
   * tgCRUD.create(sid, "", tgURI, true, "TGPR-1d223cd7-427b-7c71-c1db-54f01f8072d8", tgMeta,
   * tgData); }
   */

  /**
   * @param identifier
   * @throws IOException
   */
  public void saveToFuseki(String identifier) throws IOException {
    RDFConnection connectionToDB;
    connectionToDB = RDFConnectionFactory.connect("http://localhost:3030/subforms");
    Path path = Paths.get("/home/max/rdfTest.rdf");
    String rdfContent = Files.readString(path);

    InputStream inStream = new ByteArrayInputStream(rdfContent.getBytes());
    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, "RDF/XML");

    connectionToDB.put("textgrid:3wp6c", model);
  }

  /*
   * @Test
   * 
   * @Ignore public void testSPRQLTG() throws IOException{ Revision.getURIsFromCollection();
   * //for(String uri: Revision.getURIsFromCollection()){ // System.out.println("URI: " + uri); //}
   * }
   */

  /*
   * @Test //@Ignore public void testReadSchemaFile() { Model ttl =
   * RDFDataMgr.loadModel("C:\\dev\\sub-formsTest\\src\\main\\webapp\\bdnForms.ttl") ;
   * System.out.println("\n \n\n\n\n\n"); System.out.println(ttl.getNsPrefixMap()); }
   */

  // @Test
  // @Ignore
  /*
   * public void testSHACLValidation() { SHACL shacl = new SHACL();
   * shacl.validateDataAgainstGraph(RDFDataMgr.loadModel("src/main/webapp/bdnForms.ttl").getGraph(),
   * RDFDataMgr.loadGraph("src/main/webapp/dataexp.ttl")); }
   */

  /*
   * @Test //@Ignore public void testReadRDF() throws IOException { RDFDatabase rdf = new
   * RDFDatabase();
   * 
   * rdf.read("subforms:1tzu");
   * 
   * }
   */

  /*
   * @Test public void testGetPrefixes() { RDFDatabase rdf = new RDFDatabase();
   * System.out.println(rdf.getNamespacePrefixes()); }
   */

  /*
   * @Test() public void testGetTitleOfTGObject() {
   * 
   * }
   */

}
