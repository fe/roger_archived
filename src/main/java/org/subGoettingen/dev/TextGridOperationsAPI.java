/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 * 
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.subGoettingen.dev;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;

/**
 * Interface for the TextGridOperations function of ROGER
 * 
 * @author Maximilian Behnert-Brodhun, SUB Göttingen
 * @version 2022-07-01
 */
public interface TextGridOperationsAPI {

  /**
   * @param tgSID
   * @param tgPID
   * @param title
   * @return
   * @throws UnsupportedEncodingException
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws MalformedURLException
   */
  @GET
  @Path("/createTextGrid/{tgSID}")
  @Produces("application/ld+json")
  public String createTextGridObject(@PathParam("tgSID") String tgSID,
      @QueryParam("tgPID") String tgPID, @QueryParam("title") String title)
      throws UnsupportedEncodingException, ObjectNotFoundFault, AuthFault, IoFault,
      MetadataParseFault, MalformedURLException;

  /**
   * @param textGridUri
   * @param tgSid
   * @return
   * @throws RelationsExistFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   */
  @DELETE
  @Path("deleteTextGridObject/{uri}")
  public Response deleteTextGridObject(@PathParam("uri") String textGridUri,
      @QueryParam("tgSID") String tgSid)
      throws RelationsExistFault, ObjectNotFoundFault, AuthFault, IoFault;

  /**
   * @return
   */
  @GET
  @Path("/TGprojectIDs")
  public String getTextGridProjectID();

  /**
   * @param sid
   * @param textgridURI
   * @return
   * @throws CrudClientException
   */
  @GET
  @Path("/titleOfTextGridObject")
  public String getTitleOfTextGridObject(@QueryParam("tgSID") String sid,
      @QueryParam("tgURI") String textgridURI) throws CrudClientException;

  /**
   * @return
   */
  @GET
  @Path("/textgridstorage")
  @Produces("text/plain")
  public boolean isTextGridStorage();

  /**
   * @param tgURI
   * @param tgSID
   * @param title
   * @param jsonLD
   * @return
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws UpdateConflictFault
   */
  @POST
  @Path("/saveToTextGrid/{tgURI}")
  @Consumes("application/ld+json")
  public Response saveToTextGrid(@PathParam("tgURI") String tgURI,
      @QueryParam("tgSID") String tgSID, @QueryParam("title") String title, String jsonLD)
      throws ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault, UpdateConflictFault;

}
