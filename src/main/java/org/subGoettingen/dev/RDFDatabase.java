/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.subGoettingen.dev;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import javax.ws.rs.core.Response;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.riot.RDFDataMgr;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;

/**
 * @author Maximilian Behnert-Brodhun, SUB Göttingen
 * @version 2022-07-01
 */
public class RDFDatabase implements FormsAPI {

  private RDFConnection connectionToSPARQLEndpoint;
  private String port;
  private String URL;
  private String dataSetName;
  private String nameSpacePrefixes;
  private String objectClasses;
  private String pathToConfigeFile;
  private String schemaFile;
  private boolean useUploadDialogue;

  private static final String UTF_FORMAT = "UTF-8";
  private static final String TURTLE = "TURTLE";
  private static final String JSON_LD = "JSON-LD";
  private static final String RDF_XML = "RDF/XML";

  /**
   * @param url
   * @param port
   * @param datasetName
   * @throws MalformedURLException
   */
  public RDFDatabase(String url, String port, String datasetName) throws MalformedURLException {
    this.URL = url;
    this.port = port;
    this.dataSetName = datasetName;
    connectionToSPARQLEndpoint = RDFConnectionFactory
        .connect("http://" + getUrl() + ":" + getPort() + "/" + getDatasetName());
  }

  /**
   *
   */
  public String getQueryResults(String query) {

    ResultSet results = connectionToSPARQLEndpoint.query(query).execSelect();
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    ResultSetFormatter.outputAsJSON(outputStream, results);
    String resultsAsJSON = new String(outputStream.toByteArray());

    return resultsAsJSON;
  }

  /**
   *
   */
  public void create(String identifier, String rdfXML) {
    InputStream inStream = new ByteArrayInputStream(rdfXML.getBytes());
    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, RDF_XML);
    connectionToSPARQLEndpoint.put(identifier, model);
  }

  /**
   *
   */
  public Response delete(String identifier) {
    connectionToSPARQLEndpoint.delete(identifier);
    return Response.ok().build();
  }

  /**
   *
   */
  public String read(String identifier) throws IOException {

    // Model model = ModelFactory.createDefaultModel();
    Model model = connectionToSPARQLEndpoint.fetch(identifier);
    model.setNsPrefixes(RDFDataMgr.loadModel("http://localhost:8080/roger/" + this.getSchemaFile())
        .getNsPrefixMap());

    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    model.write(outStream, TURTLE);
    byte[] byteArray = outStream.toByteArray();

    String rdfXML = new String(byteArray, UTF_FORMAT);

    return rdfXML;
  }

  /**
   *
   */
  public String readFromUploadedFile(String identifier, String schemaFile) throws IOException {

    // Model model = ModelFactory.createDefaultModel();
    Model model = connectionToSPARQLEndpoint.fetch(identifier);
    model.setNsPrefixes(RDFDataMgr.loadModel("src/main/webapp/" + schemaFile));
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    model.write(outStream, TURTLE);
    byte[] byteArray = outStream.toByteArray();

    String rdfXML = new String(byteArray, UTF_FORMAT);

    return rdfXML;
  }

  /**
   *
   */
  public Response write(String identifier, String jsonLD) throws UnsupportedEncodingException,
      ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault, UpdateConflictFault {

    InputStream inStream = new ByteArrayInputStream(jsonLD.getBytes());
    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, JSON_LD);

    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    model.write(outStream, RDF_XML);
    byte[] byteArray = outStream.toByteArray();

    String rdfXML = new String(byteArray, UTF_FORMAT);
    create(identifier, rdfXML);

    return Response.ok().build();
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
  *
  */
  public String getSchemaFile() {
    return schemaFile;
  }

  /**
   * @param schemaFile
   */
  public void setSchemaFile(String schemaFile) {
    this.schemaFile = schemaFile;
  }

  /**
  *
  */
  public boolean isUseUploadDialogue() {
    return useUploadDialogue;
  }

  /**
   * @param useUploadDialogue
   */
  public void setUseUploadDialogue(boolean useUploadDialogue) {
    this.useUploadDialogue = useUploadDialogue;
  }

  /**
   * @return
   */
  public String getPort() {
    return port;
  }

  /**
   * @param port
   */
  public void setPort(String port) {
    this.port = port;
  }

  /**
   * @return
   */
  public String getUrl() {
    return URL;
  }

  /**
   * @param url
   */
  public void setUrl(String url) {
    this.URL = url;
  }

  /**
   * @return
   */
  public String getDatasetName() {
    return dataSetName;
  }

  /**
   * @param datasetName
   */
  public void setDatasetName(String datasetName) {
    this.dataSetName = datasetName;
  }

  /**
   * @return
   */
  public String getNamespacePrefixes() {
    return nameSpacePrefixes;
  }

  /**
   * @param namespacePrefixes
   */
  public void setNamespacePrefixes(String namespacePrefixes) {
    this.nameSpacePrefixes = namespacePrefixes;
  }

  /**
   * @return
   */
  public String getObjectClasses() {
    return objectClasses;
  }

  /**
   * @param objectClasses
   */
  public void setObjectClasses(String objectClasses) {
    this.objectClasses = objectClasses;
  }

  /**
   * @return
   */
  public RDFConnection getConnectionToSPARQLEndpoint3() {
    return connectionToSPARQLEndpoint;
  }

  /**
   * @param connectionToSPARQLEndpoint3
   */
  public void setConnectionToSPARQLEndpoint3(RDFConnection connectionToSPARQLEndpoint3) {
    this.connectionToSPARQLEndpoint = connectionToSPARQLEndpoint3;
  }

  /**
   * @return
   */
  public String getPathToConfigeFile() {
    return pathToConfigeFile;
  }

  /**
   * @param pathToConfigeFile
   */
  public void setPathToConfigeFile(String pathToConfigeFile) {
    this.pathToConfigeFile = pathToConfigeFile;
  }

}
