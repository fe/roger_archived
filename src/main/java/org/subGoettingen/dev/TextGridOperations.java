/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 * 
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.subGoettingen.dev;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.List;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.core.Response;
import javax.xml.ws.Holder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import info.textgrid.clients.CrudClient;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.clients.tgcrud.TextGridObject;
import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;


/**
 * @author Maximilian Behnert-Brodhun, SUB Goettingen
 * @version 2022-07-01
 */
public class TextGridOperations implements TextGridOperationsAPI {

  private TGCrudService tgCRUD;
  private String tG_CRUD_EndPoint;
  private String textGridProjectID;
  private boolean textGridStorage;
  private static final String UTF_FORMAT = "UTF-8";

  /**
   * @param textGridStorage
   * @param tg_CRUD_Endpoint
   * @throws MalformedURLException
   */
  public TextGridOperations(boolean textGridStorage, String tg_CRUD_Endpoint)
      throws MalformedURLException {
    this.textGridStorage = textGridStorage;
    this.tG_CRUD_EndPoint = tg_CRUD_Endpoint;
    tgCRUD = TGCrudClientUtilities.getTgcrud(this.gettG_CRUD_EndPoint());
  }

  /**
   *
   */
  @Override
  public String createTextGridObject(String sid, String tgPID, String title)
      throws UnsupportedEncodingException, ObjectNotFoundFault, AuthFault, IoFault,
      MetadataParseFault, MalformedURLException {

    ProvidedType providedMeta = new ProvidedType();
    providedMeta.setFormat("text/tg.inputform+rdf+xml");
    providedMeta.getTitle().add(title);

    GenericType genericMeta = new GenericType();
    genericMeta.setProvided(providedMeta);

    ObjectType objectMeta = new ObjectType();
    objectMeta.setGeneric(genericMeta);

    MetadataContainerType metaContainer = new MetadataContainerType();
    metaContainer.setObject(objectMeta);

    Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
    tgMeta.value = metaContainer;

    String string = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"/>";
    DataSource dataSource = new ByteArrayDataSource(string.getBytes(UTF_FORMAT), UTF_FORMAT);
    DataHandler tgData = new DataHandler(dataSource);

    tgCRUD.create(sid, "", null, false, tgPID, tgMeta, tgData);

    genericMeta = tgMeta.value.getObject().getGeneric();
    String uri = genericMeta.getGenerated().getTextgridUri().getValue();
    uri = uri.replaceFirst("\\.[0-9]*$", "");

    return "{\"@id\": \"" + uri + "\"}";
  }

  /**
   *
   */
  @Override
  public Response deleteTextGridObject(String textGridUri, String tgSid)
      throws RelationsExistFault, ObjectNotFoundFault, AuthFault, IoFault {
    tgCRUD.delete(tgSid, null, textGridUri);
    return Response.ok().build();
  }

  /**
   *
   */
  @Override
  public String getTextGridProjectID() {
    return textGridProjectID;
  }

  /**
   *
   */
  @Override
  public String getTitleOfTextGridObject(String sid, String textgridURI)
      throws CrudClientException, NullPointerException {
    CrudClient crudclient = null;
    try {
      crudclient = new CrudClient(this.gettG_CRUD_EndPoint())
          .enableGzipCompression();
    } catch (CrudClientException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    TextGridObject tgobj = crudclient.read().setSid(sid).setTextgridUri(textgridURI).execute();
    List<String> titles = tgobj.getMetadatada().getObject().getGeneric().getProvided().getTitle();

    return titles.get(0);
  }

  /**
   *
   */
  @Override
  public Response saveToTextGrid(String tgURI, String tgSID, String title, String jsonLD)
      throws ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault, UpdateConflictFault {

    MetadataContainerType metaContainer = tgCRUD.readMetadata(tgSID, "", tgURI);
    Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
    tgMeta.value = metaContainer;
    tgMeta.value.getObject().getGeneric().getProvided().getTitle().add(0, title);
    InputStream inStream = new ByteArrayInputStream(jsonLD.getBytes());

    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, "JSON-LD");

    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    model.write(outStream, "RDF/XML");
    byte[] byteArray = outStream.toByteArray();

    DataSource dataSource = new ByteArrayDataSource(byteArray, UTF_FORMAT);
    DataHandler tgData = new DataHandler(dataSource);

    tgCRUD.update(tgSID, "", tgMeta, tgData);

    return Response.ok().build();
  }

  /**
   * @param title
   * @return
   */
  public Holder<MetadataContainerType> updateTGTitle(String title) {

    ProvidedType providedMeta = new ProvidedType();
    providedMeta.setFormat("text/tg.inputform+rdf+xml");
    providedMeta.getTitle().add(title);

    GenericType genericMeta = new GenericType();
    genericMeta.setProvided(providedMeta);

    ObjectType objectMeta = new ObjectType();
    objectMeta.setGeneric(genericMeta);

    MetadataContainerType metaContainer = new MetadataContainerType();
    metaContainer.setObject(objectMeta);

    Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();

    return tgMeta;
  }

  /**
   *
   */
  @Override
  public boolean isTextGridStorage() {
    return textGridStorage;
  }

  /**
   * @param textGridStorage
   */
  public void setTextGridStorage(boolean textGridStorage) {
    this.textGridStorage = textGridStorage;
  }

  /**
   * @param textGridProjectID
   */
  public void setTextGridProjectID(String textGridProjectID) {
    this.textGridProjectID = textGridProjectID;
  }

  /**
   * @return
   */
  public String gettG_CRUD_EndPoint() {
    return tG_CRUD_EndPoint;
  }

  /**
   * @param tG_CRUD_EndPoint
   */
  public void settG_CRUD_EndPoint(String tG_CRUD_EndPoint) {
    this.tG_CRUD_EndPoint = tG_CRUD_EndPoint;
  }

}
