/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 * 
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.subGoettingen.dev;

import org.apache.jena.graph.Graph;
import org.apache.jena.shacl.ShaclValidator;
import org.apache.jena.shacl.Shapes;
import org.apache.jena.shacl.ValidationReport;

/**
 * <p>
 * Class to collect all SHACL specific functions.
 * </p>
 * 
 * @author Maximilian Behnert-Brodhun SUB Göttingen
 * @version 2022-07-01
 */
public class SHACL {

  /**
   * <p>
   * Takes a schema file and data file and checks if the data file is valid against the provided
   * schema.
   * </p>
   * 
   * @param shapesGraph contains the schema file
   * @param dataGraph contains the data file
   * 
   * @return boolean if the data file is valid against the provided schema
   */
  public boolean validateDataAgainstGraph(Graph shapesGraph, Graph dataGraph) {

    Shapes shapes = Shapes.parse(shapesGraph);
    ShaclValidator.get().parse(dataGraph);
    ValidationReport report = ShaclValidator.get().validate(shapes, dataGraph);

    /*
     * ShLib.printReport(report); RDFDataMgr.write(System.out, report.getModel(), Lang.TTL);
     */

    return report.conforms();
  }

}
