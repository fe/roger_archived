/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 * 
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.subGoettingen.dev;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import ch.qos.logback.core.spi.LogbackLock;

/**
 * 
 */
@WebServlet(name = "FileUploadServlet", urlPatterns = {"/fileuploadservlet"})
@MultipartConfig(
    fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
    maxFileSize = 1024 * 1024 * 10, // 10 MB
    maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
public class UploadFile extends HttpServlet {

  private static final long serialVersionUID = 1L;

  LogbackLock log;

  // Logger logger = LoggerFactory

  /**
   * <p>
   * Takes file from user HDD and upload into tmp directory (target/tmp).
   * </p>
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {

    String schemaPath = null;
    /*
     * Read from config file where uploaded schema should be stored This is necessary because the
     * function to save the schema file requires an absolute path
     */
    try (FileInputStream fileInputStream =
        new FileInputStream("src/main/webapp/subforms.properties")) {
      InputStream input = fileInputStream;
      Properties prop = new Properties();
      prop.load(input); // load the content from the
      schemaPath = prop.getProperty("roger_upload_dir").toString();
      input.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    /* Receive file uploaded to the Servlet from the HTML5 form */
    Part filePart = request.getPart("file");
    String fileName = filePart.getSubmittedFileName();

    for (Part part : request.getParts()) {
      /*
       * Since the directory "uploadedSchema" doesn't exist by default it has to be created
       */

      try {
        File file = new File("src/main/webapp/" + fileName);
        Files.deleteIfExists(file.toPath());
      } catch (IOException ioException) {

      }

      File theDir = new File("src/main/webapp");
      theDir.delete();
      theDir.mkdirs();
      part.write(schemaPath + fileName);
    }
  }

}
