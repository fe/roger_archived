/**
 * This software is copyright (c) 2022 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Maximilian Behnert-Brodhun (behnert-brodhun@sub.uni-goettingen.de)
 */

package org.subGoettingen.dev;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestParam;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;

/**
 * <p>
 * Interface for the triplestore functions of ROGER.
 * </p>
 * 
 * @author Maximilian Behnert-Brodhun, SUB Göttingen
 * @version 2022-07-01
 */
@Path("/")
public interface FormsAPI {

  /**
   * @param query
   * @return
   */
  @GET
  @Path("/trip")
  @Produces("text/plain")
  public String getQueryResults(@QueryParam("query") String query);

  /**
   * @return
   */
  @GET
  @Path("/schemaFile")
  @Produces("text/plain")
  public String getSchemaFile();

  /**
   * @return
   */
  @GET
  @Path("/useUploadDialogue")
  @Produces("text/plain")
  public boolean isUseUploadDialogue();

  /**
   * @param identifier
   * @param rdfXML
   */
  @POST
  @Path("/create")
  @Produces("application/ld+json")
  public void create(@RequestParam("identifier") String identifier, String rdfXML);

  /**
   * @param identifier
   * @return
   * @throws UnsupportedEncodingException
   * @throws IOException
   */
  @GET
  @Path("/read/{identifier}")
  @Produces("text/turtle")
  public String read(@PathParam("identifier") String identifier)
      throws UnsupportedEncodingException, IOException;

  /**
   * @param identifier
   * @param schemaFile
   * @return
   * @throws IOException
   */
  @GET
  @Path("readFromUpload/{identifier}")
  @Produces("text/turtle")
  public String readFromUploadedFile(@PathParam("identifier") String identifier,
      @QueryParam("schema") String schemaFile) throws IOException;

  /**
   * @param identifier
   * @param jsonLD
   * @return
   * @throws UnsupportedEncodingException
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws UpdateConflictFault
   */
  @POST
  @Path("/update/{identifier}")
  @Consumes("application/ld+json")
  public Response write(@PathParam("identifier") String identifier, String jsonLD)
      throws UnsupportedEncodingException, ObjectNotFoundFault, AuthFault, IoFault,
      MetadataParseFault, UpdateConflictFault;

  /**
   * @param identifier
   * @return
   */
  @DELETE
  @Path("/delete/{identifier}")
  public Response delete(@PathParam("identifier") String identifier);

}
