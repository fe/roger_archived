var textGridStorage;
var uploadDialogue;
var schemaFile;
var shacl = new subFormsSHACL();

/**
 * Uploads a selected schema file and calls the upload function from Java-API to store it
 * @returns an alert to give user feedback about success in uploading the file
 */
async function uploadFile() {
  let formData = new FormData();
  schemaFile = ajaxfile.files[0]["name"];
  formData.append("file", ajaxfile.files[0]);
  await fetch("fileuploadservlet", {
    method: "POST",
    body: formData,
  });
  alert("The file upload with Ajax and Java was a success!");
}

/**
 * Generates the formula corresponding to the formulaType
 * By call a new object will be created
 */
function generateForm(formularType, openObject) {
  createNewObject(openObject);

  if (getUploadDialogue() === "false") {
    schemaFile = "";
  }
  return $.ajax({
    type: "GET",
    url: schemaFile,
    //url: "bdnForms.ttl",
    mimeType: "text/turtle",
  }).done(function (data) {
    var addCall;
    addCall = function () {
      var contentCount = parseInt($("li.nav-item").length) - 1;
      return shacl.buildForm(formularType, "div#content" + contentCount);
    };
    shacl.addTurtle(data, addCall);
  });
}

/**
 * Generic save function of roger.
 * Check if all mandatory field are filled and saving
 * files in triplestore and also in TextGrid-Repository if it is used
 */
function save(uriToSave, contentTabID) {
  if (checkAllMandatoryFields(contentTabID.replace("-tab", ""))) {
    var formularTypeToSave = $(
      "div#" + contentTabID.replace("-tab", "") + " form"
    ).attr("name");
    var tmpFormName = shacl
      .getStore()
      .find(null, "sh:targetClass", formularTypeToSave);
    var objectType = shacl
      .getStore()
      .find(shacl.abbrevURI(tmpFormName[0].subject), "sh:targetClass", null);
    var contentToSave = shacl.getInput(
      uriToSave,
      objectType[0].object,
      "div#" + contentTabID.replace("-tab", "")
    );
    var dataToSave = [contentToSave];
    var subformularRef = $(".subformular");

    for (let i = 0, len = subformularRef.length; i < len; i++) {
      var meta = subformularRef[i];
      var subformularName = $(".subformular")
        .parent()
        .parent()
        .attr("data-tgforms-name");
      var metadata = shacl.getInput(
        meta.id.replace("_", ":"),
        subformularName,
        "div#" + meta.id
      );
      dataToSave.push(metadata);
    }
    dataToSave = JSON.stringify(dataToSave);
  }

  if (textGridStorage === "false") {
    saveToTriplestore(uriToSave, dataToSave);
  } else {
    saveToTriplestore(uriToSave, dataToSave);
    saveToTextGrid(uriToSave, contentTabID.replace("-tab", ""), dataToSave);
  }
}

/**
 * Sends GET-Request to the endpoint and fills the corresponding form with the requested data
 */
//TODO rename "open"
function open(val) {
  //If information about the last opened object is still there delete it
  if ($("div#openedObject").length > 0) {
    $("div.uriInfo").empty();
  }
  var uriToOpen = val.replace(/\.[0-9]*$/, "");
  //TODO: check variable uri
  var url = "";
  if (getUploadDialogue() === "false") {
    url = "db/read/" + uriToOpen;
  } else {
    url = "db/readFromUpload/" + uriToOpen + "?schema=" + schemaFile;
  }

  return $.ajax({
    type: "GET",
    url: url,
    mimeType: "text/turtle",
    data: shacl.getPrefixes(),
    cache: false,
  }).done(function (data) {
    var addCall;
    addCall = function () {
      var fillForm;
      var type = shacl.getType(uriToOpen);
      generateForm(type, true);
      if ($("div.uriInfo").children().length > 0) {
        $("div.uriInfo").children().remove();
      }
      $("div.uriInfo").append(
        '<div id="openedObject" class="alert alert-info" role="alert">You are editing the object: <strong>' +
          uriToOpen +
          "</strong><br/> with the type: <strong>" +
          type +
          "</strong></div>"
      );
      fillForm = function () {
        var contentTabID = parseInt($("li.nav-item").length) - 1;
        shacl.fillForm(uriToOpen, "div#content" + contentTabID);
        //Get content of subformular and load it
        nameAddedTab(contentTabID, uriToOpen);
        //If the TextGrid-Repository is used it is necessary to wait for the title and set it from the TextGrid-Metadata object
        if (textGridStorage === "true") {
          $("div#content" + contentTabID + " fieldset.title").prepend(
            '<div class="form-group row textgridTitle"><label for="textGridTitle" class="col-sm-2 col-form-label">TG-Title</label><div class="col-sm-10"><input type="text" class="form-control" id="textGridTitle" placeholder="Title in TextGrid"></div></div>'
          );
          var waitForTextGridTitle = new Promise(function (resolve) {
            window.setTimeout(function () {
              resolve(getTitleOfTextGridObject(uriToOpen));
            }, Math.random() * 1000);
          });
          waitForTextGridTitle.then(function () {
            //TODO: check whats about textgridTitle
            $("div#content" + contentTabID + " input#textGridTitle").val(
              textgridTitle
            );
          });
        }
        loadSubFormulars();
      };
      return setTimeout(fillForm, 1000);
    };
    return shacl.addTurtle(data, addCall);
  });
}

/**
 * Creates a new object and open a formula in a new tab named by an URI
 */
function createNewObject(openExistingObject) {
  //If saveInfo from last object is already existing delete it.
  if ($("div.savedObject").length > 0) {
    $("div.saveInfo").empty();
  }
  //If information about the last opened object is still there delete it
  if ($("div#openedObject").length > 0) {
    $("div.uriInfo").empty();
  }

  /*Add new tab to object navigation*/
  var contentCount = $("li.nav-item").length;
  var iconSave =
    '<span class="objectFunctions saveobject resource fa fa-save icon-save-file" aria-hidden="true"></span>';
  var iconDeleteObject =
    '<span class="objectFunctions delete resource fa fa-trash icon-trash" aria-hidden="true"></span>';
  var iconCloseTab =
    '<span class="objectFunctions closeTab resource fa fa-remove icon-remove" aria-hidden="true"></span>';
  var iconsForObjectFunction = iconSave + iconDeleteObject + iconCloseTab;
  var spinnerTmpForURIWaiting =
    '<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>';
  var uri;

  $("ul.nav.nav-tabs").append(
    '<li name="content' +
      contentCount +
      '" class="nav-item" role="presentation">' +
      '<button class="nav-link active" id="content' +
      contentCount +
      '-tab" data-bs-toggle="tab" data-bs-target="#content' +
      contentCount +
      '" type="button" role="tab" aria-controls="content' +
      contentCount +
      '" aria-selected="true"><span class="uri">' +
      spinnerTmpForURIWaiting +
      "</span>" +
      iconsForObjectFunction +
      "</button>" +
      "</li>"
  );

  $("div.tab-content").append(
    '<div class="tab-pane fade active show" id="content' +
      contentCount +
      '" role="tabpanel" aria-labelledby="content' +
      contentCount +
      '-tab"></div>'
  );
  $("div#content" + parseInt(contentCount - 1)).removeClass("active");
  $("div#content" + parseInt(contentCount - 1)).removeClass("show");
  $("button#content" + parseInt(contentCount - 1) + "-tab").attr(
    "aria-selected",
    "false"
  );
  $("button#content" + parseInt(contentCount - 1) + "-tab").removeClass(
    "active"
  );
  $("button#content" + parseInt(contentCount - 1) + "-tab").removeClass("show");

  if (!openExistingObject) {
    if (textGridStorage === "false") {
      uri = generateURI();
      nameAddedTab(contentCount, uri);
    } else {
      var textgridProjectID = $("button#textGridProjectChoice").text();
      textgridProjectID = textgridProjectID.substring(textgridProjectID.indexOf(":")+1, textgridProjectID.length);
      console.log(textgridProjectID);
      //The creation process of a new TextGrid-URI takes a while
      //so here a promise is necessary to get a TextGrid-URI and have a value for the uri variable
      var waitForCreationOfNewTextGridObject = new Promise(function (resolve) {
        window.setTimeout(function () {
          resolve(
            
            createNewTextGridObject(              
              textgridProjectID
            )
          );
        }, Math.random() * 2000 + 1000);
      });
      waitForCreationOfNewTextGridObject.then(function () {
        uri=getTGUri();
        nameAddedTab(contentCount, uri);
        $("div#content" + contentCount + " fieldset.title").prepend(
          '<div class="form-group row textgridTitle"><label for="textGridTitle" class="col-sm-2 col-form-label">TG-Title</label><div class="col-sm-10"><input type="text" class="form-control" id="textGridTitle" placeholder="Title in TextGrid"></div></div>'
        );
      });
    }
  }
}

function nameAddedTab(contentCount, uri) {
  $("a[href='#content" + contentCount + "'] div.spinner-border").remove();
  $("button#content" + contentCount + "-tab span.uri").text(uri);
}

/**
 * This function generates the form for a subform
 */
function addMdForm(fileId, fieldName, formularToLoad) {
  console.log("TADA");
  var className, divId, fdiv;
  divId = fileId.replace(":", "_");
  className = fileId.split(":");
  
  fdiv = $(
    '<div id="' +
      divId +
      '" class="' +
      className[1] +
      ' subformular in "testitest">content</div>'
  );
  $("span:contains(" + fileId + ")")
    .parent()
    .append(fdiv);
  var contentCount = parseInt($("li.nav-item").length) - 1;
  shacl.buildForm(formularToLoad, "div#content" + contentCount + " " + "div#" + divId);
  return shacl.fillForm(fileId, "div#content" + contentCount + " " + "div#" + divId);
}

/**
 * if the button in the formula triggers a sub-formula
 * this function adding it the formula to the main-formula
 */
function loadSubFormulars() {
  
  $("button.formularOp").each(function () {
    if ($(this).prev().text().length > 0) {
      //TODO: remove "tgforms" for attribute name
      var formName = $(this).parent().parent().attr("data-tgforms-name");
      formName = shacl.getStore().find(null, "sh:path", formName);
      formName = formName[0].subject;
      formName = shacl.abbrevURI(formName);
      var partId = $(this).prev().text();

      var formularToLoad = shacl.getStore().find(formName, "sh:node", null);
      console.log(formularToLoad[0].subject);
      addMdForm(partId, formName, formularToLoad[0].object);
      loadNextLayer(partId.replace(":", "_"));
    }
  });
}
/**
 * If a button triggers a sub-formula inside a sub-formula
 * this function loads all next layers
 */
function loadNextLayer(divId) {
  $("div#" + divId + " button.formularOp").each(function () {
    
    if ($(this).prev().text().length > 0) {
      //TODO: remove "tgforms" for attribute name
      var formName = $(this).parent().parent().attr("data-tgforms-name");
      //var ref = shacl.getStore().find(identifierGlobal, formName, null);
      //var triple = ref[0];
      var partId = $(this).prev().text();
      var formularToLoad = shacl.getStore().find(formName, "sh:node", null);
      addMdForm(partId, formName, formularToLoad[0].object);
      if (
        $("div#" + partId.replace(":", "_") + " button.formularOp").length > 0
      ) {
        loadNextLayer(partId.replace(":", "_"));
      }
    }
  });
}

/**
 * Returns a list of all objects which are classes (sh:NodeShape)
 * AND has the property: tgforms:FormNode true
 */
function getFormularClasses() {
  return $.ajax({
    type: "GET",
    url: schemaFile,
    mimeType: "text/turtle",
  }).done(function (data) {
    addCall = function () {
      return shacl.getFormularClasses();
    };
    shacl.addTurtle(data, addCall);

    return shacl.getFormularClasses();
  });
}

/**
 * Checks if all mandatory fields are filled out.
 * Buttons, textareas, input fields, dropdowns and checkboxes can e mandatory
 */

function checkAllMandatoryFields(divContentId) {
  if (checkMandatory(divContentId) && checkMandatoryButton(divContentId) && checkMandatoryDropdown(divContentId)) {
    return true;
  } else {
    return false;
  }
}

/**
 * Clearing the modals result-list of a search result
 */
function clearModalBody(modalName) {
  $("div#" + modalName + "List").empty();
}

/**
 * sends a request to the ROGER-API to get the information if the TextGrid-Repository is used as storage endpoint
 */

function isTextGridStorage() {
  return $.ajax({
    type: "GET",
    url: "db/tgOp/textgridstorage/",
    mimeType: "text/plain",
  }).done(function (data) {
    setTextGridStorage(data);
    return textGridStorage;
  });
}

/**
 * sends a request to the ROGER-API to get the information if the upload dialogue for schema files is used or not
 */

async function isUploadDialogue() {
  return $.ajax({
    type: "GET",
    url: "db/useUploadDialogue",
    mimeType: "text/plain",
  }).done(function (data) {
    setUploadDialogue(data);
    return uploadDialogue;
  });
}

/**
 * Setter for the boolean if the upload dialogue is used or not
 * @param {} val
 * @returns
 */

function setUploadDialogue(val) {
  return (uploadDialogue = val);
}

function getUploadDialogue() {
  return uploadDialogue;
}

function setSchemaFile() {
  return $.ajax({
    type: "GET",
    url: "db/schemaFile",
    mimeType: "text/plain",
  }).done(function (data) {
    schemaFile = data;
    return schemaFile;
  });
}

function getSchemaFile() {
  return schemaFile;
}

/**
 * CLICK EVENT HANDLER
 */

/**
 * Opens the modal for the object type to be generated
 * @returns
 */
$(document).on("ready click", "button#chooseobject", function () {
  $("div#classPicker").modal("show");
  getFormularClasses();
  if (
    textGridStorage === "true" &&
    $("div.dropdown.textGridProjectID").length < 1
  ) {
    getTGprojectIDs();
  }
});

/**
 *
 * @returns
 */
$(document).on(
  "ready click",
  "div#classPicker button.classSelection",
  function () {
    if (
      textGridStorage === "false" ||
      (textGridStorage == "true" &&
        $("button#textGridProjectChoice").text() !== "TG-Project ID")
    ) {
      $("div#classPicker").modal("hide");
      $(this).unbind("click");
      return generateForm($(this).attr("name"), false);
    } else {
      alert("Please choose a TextGrid-Project from the menu");
    }
  }
);

/**
 * If a button for searchOp in a triplestore is clicked the search in project
 * triplestore will be queried automatically with a unspecified keyword.
 *
 * The search by a keyword is also possible
 *
 * @returns
 */
$(document).on("ready click", "button.searchOp", function () {
  $("div#triplestoreResult").modal("show");
  $("div#triplestoreResult input#tripleStoreOffset").val(0)
  $("div#triplestoreResult button#objectChoice").text("Set");
  $("div#triplestoreResult button#objectChoice").attr(
    "id",
    "set-TripstoreURI"
  );
  console.log($(this).attr("remotesearch"));
  if($(this).attr("remotesearch")==="true"){
    $("button#filterSearch").remove();
    if($("input#objectSearch-keyword").length < 1){
      $("div#triplestoreResult div.container-fluid").append(
        "<div class=\"input-group mb-3\">" +
                          "<input id=\"objectSearch-keyword\" type=\"text\" class=\"form-control\" placeholder=\"keyword\" aria-label=\"keyword\" aria-describedby=\"basic-addon2\">" +
                          "<span id=\"performRemoteSearch\" type=\"button\" class=\"input-group-text\">Search</span>" +
                       "</div>"
      );
    }
    currentField = $(this).closest("div.form-group");
    remoteQuery($(this).attr("remoteAddress"));
  }else{
    $("button#filterSearch").attr("name", $(this).attr("name"));
    $("div#triplestoreResult input#tripleStoreOffset").val("0");
    $("div.queryFilterContainer").remove();
    if($("button#filterSearch").length <1){
      $("div#triplestoreResult div.container-fluid").append("<button id=\"filterSearch\" type=\"button\">Filter</button>");
    }    
    currentField = $(this).closest("div.form-group");
    var prefixes = shacl.getPrefixes();
    var prefixesForTripleStore = "";
    for (var prefix in prefixes) {
      prefixesForTripleStore =
        prefixesForTripleStore +
        "PREFIX " +
        prefix +
        ": <" +
        prefixes[prefix] +
        ">";        
    }
    buildFilterDropdown($(this).attr("name"));
    setPredicateList(prefixesForTripleStore, $(this).attr("name"));
    
    
  }
});

/**
 * Takes the URI of the corresponding object from the button text and calling save function
 * @returns
 */

$(document).on("ready click", "span.saveobject", function () {
  /*
   * The save function needs to know in which tab the content for saving is given. So this click handler
   * take the URI and the id of the tab from the navigation
   */

  var idToSave = $(this).parent().attr("id").replace("#", "");
  //Get URI from span.class inside <button></button> from the tab navigation
  var uriToSave = $(this).prev().text();
  $(this).replaceWith(
    '<div class="spinner-border spinner-border-sm" role="status">' +
      '<span class="sr-only">Loading...</span>' +
      "</div>"
  );
  save(uriToSave, idToSave);
});

/**
 * Takes the URI of the corresponding object from the button text and calling delete function
 */
$(document).on("ready click", "span.delete", function () {
  //Get URI from span.class inside <button></button> from the tab navigation
  var uriToDelete = $(this).prev().prev().text();
  $("div#deleteAlert div#uriToDelete div.alert-danger").text(uriToDelete);
  $("div#deleteAlert").modal("show");
  $(document).on(
    "ready click",
    "div#deleteAlert button.deleteObject",
    function () {
      if (textGridStorage === "true") {
        deleteTextGridObject(uriToDelete);
        deleteTripleStoreGraph(uriToDelete);
      } else {
        deleteTripleStoreGraph(uriToDelete);
      }
    }
  );
});

/**
 * Checks if an URI is entered in the input field and alert if no URI is given *
 *
 */
$(document).on("ready click", "span#openDirectURI", function () {
  var uri = $("input#openDirectURI-input").val();
  if (uri.length > 0) {
    open(uri);
  } else {
    alert("Please enter an URI to open object");
  }
});

/**
 * Triggers the modal to open an object from the triplestore in several possibilities
 */
$(document).on("ready click", "button#openobject", function () {
  $("div#openDataObject").modal("show");

  $("div.radio").remove();
  getFormularClasses();
});

/**
 * Close the corresponding tab of the navigation and
 * makes the last tab in the navigation the active one
 */
$(document).on("ready click", "span.closeTab", function () {
  var contentCount = parseInt($("li.nav-item").length) - 1;
  var idToClose = $(this).parent().attr("id");
  $('li[name="' + idToClose.replace("-tab", "") + '"]').remove();
  $("div#" + idToClose.replace("-tab", "")).remove();
  var contentCountForActivateNextTab = parseInt(contentCount) - 1;
  $("div#content" + contentCountForActivateNextTab).attr(
    "class",
    "tab-pane fade active show"
  );
  $("button#content" + contentCountForActivateNextTab + "-tab").attr(
    "class",
    "nav-link active"
  );
  $(this).unbind("click");
});

/**
 * Takes the URI of the chosen object from a search result (get if from the clicked radio)
 * and calls the open function with this URI as parameter
 * @returns an opened object specified by URI
 */
$(document).on(
  "ready click",
  "div#triplestoreResult button#objectChoice",
  function () {
    var subject = $(
      'div#triplestoreResult div#triplestoreResultList input[name="objectMetadata-radios"]:checked'
    ).val();
    return open(subject);
  }
);

/**
 * Triggers the search in the tripelstore for objects.
 *
 * @returns a result list of a SPARQL request
 */
$(document).on(
  "ready click",
  "div#openDataObject button.classSelection",
  function () {
    $("div.filterSettings").remove();
    $("div#triplestoreResult").modal("show");
    $("div#triplestoreResult input#tripleStoreOffset").val(0)
    /*
     * This modal is used by the search functions for a linking a object out of the forms.
     * In this case button for processing is named "Set" but in this context the name has to be "Open"
     * The id of the button has to be set to 'objectChoice' since this triggers a different function
     * then it the default case of the modal
     */
    $("div#triplestoreResult input#tripleStoreOffset").val("0");
    $("div#triplestoreResult button#set-TripstoreURI").text("Open");
    $("div#triplestoreResult button#set-TripstoreURI").attr(
      "id",
      "objectChoice"
    );
    var prefixes = shacl.getPrefixes();
    var prefixesForTripleStore = "";
    for (var prefix in prefixes) {
      prefixesForTripleStore =
        prefixesForTripleStore +
        "PREFIX " +
        prefix +
        ": <" +
        prefixes[prefix] +
        ">";
    }
    var objectClass = $(this).attr("name");
    $("div.filterSettings").remove();
    $("button#filterSearch").attr("name", objectClass);
    buildFilterDropdown(objectClass);
    $("div#openDataObject button[name='" + objectClass + "']")
      .parent()
      .after()
      .append(
        '<div id="objectList-' + objectClass.replace(":", "_") + '"></div>'
      );
    setPredicateList(prefixesForTripleStore, objectClass);
  }
);


/**
 * returning a list of TextGrid-Project IDs.
 * In the context menu of "create new object" it is possible to choose between all project for saving.
 *
 * contentTabID: ID of the tab
 */

 function getTGprojectIDs() {
  return $.ajax({
    type: "GET",
    url: "db/tgOp/TGprojectIDs",
    mimeType: "text/plain",
  }).done(function (data) {
    if ($("div.dropdown.textGridProjectID").length == 0) {
      $("div#classPicker div.modal-footer").append(
        '<div class="dropdown textGridProjectID">' +
          '<button class="btn btn-secondary dropdown-toggle" type="button" id="textGridProjectChoice" data-bs-toggle="dropdown" aria-expanded="false">TG-Project ID</button>' +
          '<ul class="dropdown-menu" aria-labelledby="textGridProjectChoice">' +
          "</ul>" +
          "</div>"
      );
    }
    let projectIDs = data;
    /*
     * The list of the projectIDs entered in config file is sperated by comma.
     * To get the single entries this string will be splitted.
     */
    var array = projectIDs.split(",");
    /*
     * For each project ID an entry in the drop down will be generated.
     */
    array.forEach(function (item) {
      $("div.dropdown.textGridProjectID ul.dropdown-menu").append(
        '<li><a class="dropdown-item" href="#">' + item + "</a></li>"
      );
    });
    return data;
  });
}

/**
 * Delete the object in the TextGrid-Repository identified by an URI
 *
 * @param {String} uri: identifier in the TextGrid-Repository
 */

 function deleteTextGridObject(uri) {
  return $.ajax({
    type: "DELETE",
    url:
      "db/tgOp/deleteTextGridObject/" + uri + "?tgSID=" + 
      getSidFromURLParam(),
    mimeType: "text/plain",
  }).done(function () {
    alert(uri + " is deleted in TextGridRepository");
    return window.close();
  });
}