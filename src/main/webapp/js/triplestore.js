/**
 * http://usejsdoc.org/
 */
var currentField = {};

function saveToTriplestore(uriToSave, data) {
  console.log(data);
  console.log(uriToSave);
  $("div.savingstatus").text("Storing object in Triplestore");
  return $.ajax({
    type: "POST",
    url: "db/update/" + uriToSave,
    contentType: "application/ld+json",
    data: data,
    cache: false,
  }).done(function () {
    var iconSave =
      '<span class="objectFunctions saveobject resource fa fa-save icon-save-file" aria-hidden="true"></span>';
    $("div.spinner-border.spinner-border-sm").replaceWith(iconSave);
    $("div.saveInfo").append(
      '<div class="alert alert-info savedObject" role="alert">Object was saved with the URI: <strong>' +
        uriToSave +
        "</strong> in the triplestore" +
        "</div>"
    );
  });
};

/**
 *
 */
deleteTripleStoreGraph = function (identifier) {
  return $.ajax({
    type: "DELETE",
    url: "db/delete/" + identifier,
    mimeType: "text/plain",
  }).done(function () {
    alert(identifier + " is deleted in Triplestore");
    return window.close();
  });
};

/**
 * Takes from a marked object in a result-list and extracts the URI
 * of the object to link it with the current object the user is editing
 */
function setTheId (currentField) {
  //TODO: div#triplestoreResultList to queryResultList
  //TODO: redeem the currentField ugliness
  var id = $(
    'div#triplestoreResultList input[name="objectMetadata-radios"]:checked'
  ).val();
  if (id){
    currentField.find("span.value + span").remove();
    currentField.find("span.value").text(id);
  }
  $("div#triplestoreResultList").empty();
  return $("div#triplestoreResult").modal("hide");
};

/**
 * Generates an URI for the object and to name the name if TextGrid-Repository is not used
 */
function generateURI() {
  var chars, result, today;
  chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  today = new Date();
  result = today.valueOf().toString(8);
  result += chars.substr(Math.floor(Math.random() * chars.length), 1);
  //TODO: subforms is a preset for prefix. It should be come from the config of the project
  return "roger:" + result;
};

/**
 * CLICK EVENT HANDLER
 */

/**
 * Sets the link to an object chosen by the marking of
 * a radio in the result list of a query. Calls the setTheId(currentField) Function
 *
 * @returns
 */
 $(document).on("ready click", "button#set-TripstoreURI", function () {
  $("div.queryFilterContainer").remove();
  setTheId(currentField);
});

/**
 * By closing the context-menu of a query-search the result list is emptied
 *
 * @returns
 */
$(document).on(
  "ready click",
  "div#triplestoreResult button.btn-close",
  function () {
    $("div.queryFilterContainer").remove();
    $("div#triplestoreResultList").empty();
  }
);
