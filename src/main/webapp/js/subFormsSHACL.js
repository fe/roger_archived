var subFormsSHACL;

subFormsSHACL = (function () {
  var abbrevURI,
    addToJSONLD,
    clearForm,
    deleteReplace,
    divid,
    expandPrefix,
    generateId,
    getClasses,
    getDivId,
    getFieldHTML,
    getFieldObject,
    getFormTriples,
    getList,
    getListStart,
    getPrefixes,
    getUnionOf,
    helptext,
    isResource,
    labelSearch,
    mandatoryReplace,
    parser,
    prefixCall,
    repeatField,
    repeatReplace,
    resourceReplace,
    sortFields,
    store,
    templates,
    util;

  function subFormsSHACL() {}

  templates = {};

  templates["subFormsSHACL:button"] =
    '<div class="form-group  tgforms:button {{ sh:path }}" data-tgforms-type="tgforms:button" data-tgforms-name="{{ sh:path }}">\n <span class="label">{{ sh:name }}</span>\n {{#sh:description}}\n <span title="{{.}}" class="glyphicon glyphicon-info-sign icon-info"></span>\n {{/sh:description}}\n <div>\n <span class="value">{{ tgforms:hasDefault }}</span>\n \n {{#tgforms:buttonID}}\n {{#tgforms:isMandatory}}\n <button type="button" class="btn btn-secondary formularOp" id="{{tgforms:buttonID}}" required>Choose</button>\n <button type="button" class="btn btn-secondary {{tgforms:buttonID}} removeFormular" disabled="disabled">Remove</button>\n {{/tgforms:isMandatory}}\n {{^tgforms:isMandatory}}\n <button type="button" class="btn btn-secondary formularOp" id="{{tgforms:buttonID}}">Choose</button>\n <button type="button" class="btn btn-secondary {{tgforms:buttonID}} removeFormular" disabled="disabled">Remove</button>\n {{/tgforms:isMandatory}}\n {{/tgforms:buttonID}}	\n {{^tgforms:buttonID}}\n {{#tgforms:isMandatory}}\n <button type="button" class="btn btn-secondary searchOp" required name="{{sh:class}}">Choose</button>\n <button type="button" class="btn btn-secondary removeContent" disabled="disabled">Remove</button>\n {{/tgforms:isMandatory}}\n {{^tgforms:isMandatory}}\n <button type="button" class="btn btn-secondary searchOp bla {{sh:class}}" remoteSearch={{roger:remoteSearch}} remoteAddress="{{roger:remoteAddress}}" name="{{sh:class}}">Choose</button>\n <button type="button" class="btn btn-secondary removeContent" disabled="disabled">Remove</button>\n {{/tgforms:isMandatory}}\n {{/tgforms:buttonID}}\n \n </div>\n </div>';

  templates["subFormsSHACL:checkbox"] =
    '<div class="form-group tgforms:checkbox {{ sh:path }}" data-tgforms-type="tgforms:checkbox" data-tgforms-name="{{ sh:path }}">\n \n <label>\n {{#tgforms:hasFieldFunction}}\n {{#tgforms:isMandatory}}\n <input type="checkbox" style="width:16px;height:16px;" onblur="{{.}}" required >\n {{/tgforms:isMandatory}}\n {{^tgforms:isMandatory}}\n <input type="checkbox" style="width:16px;height:16px;" onblur="{{.}}" >\n {{/tgforms:isMandatory}}\n {{/tgforms:hasFieldFunction}}\n {{^tgforms:hasFieldFunction}}\n {{#tgforms:isMandatory}}\n <input type="checkbox" style="width:16px;height:16px;" required>\n {{/tgforms:isMandatory}}\n {{^tgforms:isMandatory}}\n <input type="checkbox" style="width:16px;height:16px;">\n {{/tgforms:isMandatory}}    	\n {{/tgforms:hasFieldFunction}}\n <span class="label">{{ sh:name }}</span>\n {{#rdfs:comment}}\n <span title="{{.}}" class="glyphicon glyphicon-info-sign icon-info"></span>\n {{/rdfs:comment}}   \n \n </label>  \n </div>';

  templates["subFormsSHACL:dropdown"] =
    '<div class="dropdown form-group subFormsSHACL:dropdown {{ sh:path }}" data-tgforms-type="subFormsSHACL:dropdown" data-tgforms-name="{{ sh:path }}">\n <span class="label">{{ sh:name }}</span>\n {{#sh:description}}\n <span title="{{.}}" class="glyphicon glyphicon-info-sign icon-info"></span>\n {{/sh:description}}\n {{#tgforms:isMandatory}}  \n <button id="{{sh:PropertyShape}}" type="button" class="btn btn-secondary dropdown-toggle formDD" data-bs-toggle="dropdown" aria-expanded="false" required>\n <span class="value">{{ tgforms:hasDefault }}</span><span class="caret"></span>\n </button>\n {{/tgforms:isMandatory}}\n {{^tgforms:isMandatory}}\n <button id="{{sh:PropertyShape}}" type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown"><span class="value">{{ tgforms:hasDefault }}</span><span class="caret"></span></button>{{/tgforms:isMandatory}}<ul class="dropdown-menu" aria-labelledby="{{ sh:PropertyShape }}">\n {{#tgforms:hasOption}}\n <li><a class="dropdown-item formDDItem value" href="javascript:return false">{{.}}</a></li>{{/tgforms:hasOption}}</ul></div>';

  templates["subFormsSHACL:hidden"] =
    '<div class="form-group tgforms:hidden {{ rdf:Property }} hidden" data-tgforms-type="tgforms:hidden" data-tgforms-name="{{ rdf:Property }}">\n <label>\n <span class="label">{{ rdfs:label }}</span>\n <input type="text" class="form-control">\n </label>\n </div>';

  templates["subFormsSHACL:text"] =
    '<div class="form-group tgforms:text {{ sh:path }}" data-tgforms-type="tgforms:text" data-tgforms-name="{{ sh:path }}">\n <label>\n <span class="label">{{ sh:name }}</span>\n \n {{#sh:description}}\n <span title="{{.}}" class="glyphicon glyphicon-info-sign icon-info"></span>\n {{/sh:description}}\n \n {{#tgforms:hasDefault}}\n <input type="text" class="form-control" value="{{.}}">\n {{/tgforms:hasDefault}}\n \n \n {{#tgforms:hasFieldFunction}}\n {{#tgforms:isMandatory}}    	\n <input type="text" onblur="{{.}}" class="{{sh:PropertyShape}} form-control" required>\n {{/tgforms:isMandatory}}\n {{^tgforms:isMandatory}}\n <input type="text" onblur="{{.}}" class="{{sh:PropertyShape}} form-control" >\n {{/tgforms:isMandatory}}\n {{/tgforms:hasFieldFunction}}\n {{^tgforms:hasFieldFunction}}\n {{#tgforms:isMandatory}}\n <input id="{{sh:PropertyShape}}" type="text" class="form-control" required>\n {{/tgforms:isMandatory}}\n {{^tgforms:isMandatory}}\n <input id="{{sh:PropertyShape}}" type="text" class="form-control">\n {{/tgforms:isMandatory}}    	\n {{/tgforms:hasFieldFunction}}\n \n </label>\n <span>\n </div>';

  templates["subFormsSHACL:textarea"] =
    '<div class="form-group tgforms:textarea {{ sh:path }}" data-tgforms-type="tgforms:textarea" data-tgforms-name="{{ sh:path }}">\n <label>\n <span class="label">{{ sh:name }}</span>\n {{#sh:description}}\n <span title="{{.}}" class="glyphicon glyphicon-info-sign icon-info"></span>\n {{/sh:description}}\n <textarea class="form-control" rows="5"></textarea>\n </label>\n \n </div>';

  labelSearch = new RegExp('<span class="label">(.*)</span>');
  resourceReplace =
    '<span class="label">$1</span><span class="icon resource ' +
    'fa fa-link icon-link" aria-hidden="true"></span>';
  repeatReplace =
    '<span class="label">$1</span><span class="icon repeat ' +
    'fa fa-plus icon-plus" aria-hidden="true"></span>';
  deleteReplace =
    '<span class="label">$1</span><span class="icon deleteField ' +
    'fa fa-minus icon-minus" aria-hidden="true"></span>';
  mandatoryReplace =
    '<span class="label">$1</span><span class="icon mandatory ' +
    'fa fa-asterisk icon-asterisk" aria-hidden="true"></span>';

  parser = N3.Parser();
  store = N3.Store();
  util = N3.Util;
  divid = "";
  helptext = "";

  abbrevURI = function (string) {
    var prefix, ref, uri;
    ref = getPrefixes();
    for (prefix in ref) {
      uri = ref[prefix];
      string = string.replace(uri, prefix + ":");
    }
    return string;
  };

  addToJSONLD = function (jsonLD, domObject) {
    var key, newValue, oldValue;
    if (domObject.attr("type") === "checkbox") {
      if (domObject.prop("checked")) {
        newValue = true;
      } 
    }
    if (domObject.attr("type") === "text") {
      if (domObject.val()) {
        newValue = domObject.val();
      } 
    }
    if (domObject.prop("tagName") === "SPAN") {
       if (domObject.text()) {
        newValue = domObject.text();
      }
    }
    if (domObject.prop("tagName") === "TEXTAREA") {
      if (domObject.val()) {
        newValue = domObject.val();
      }
    }
    key = domObject.closest("div.form-group").attr("data-tgforms-name");
    if (isResource(key)) {
      newValue = {
        "@id": newValue,
      };
    }
    oldValue = jsonLD[key];
    if (oldValue instanceof Array) {
      jsonLD[key].push(newValue);
    } else if (oldValue) {
      jsonLD[key] = [oldValue, newValue];
    } else {
      jsonLD[key] = newValue;
    }
    return jsonLD;
  };

  expandPrefix = function (string) {
    return util.expandPrefixedName(string, getPrefixes());
  };

  getClasses = function (subject) {
    var i,
      j,
      len,
      len1,
      rdfClass,
      rdfClasses,
      ref,
      subClassOfTriple,
      subClassOfTriples;

    rdfClasses = [subject];
    subClassOfTriples = store.find(subject, "rdfs:subClassOf", null);

    for (i = 0, len = subClassOfTriples.length; i < len; i++) {
      subClassOfTriple = subClassOfTriples[i];
      rdfClass = abbrevURI(subClassOfTriple.object);
      rdfClasses.push(rdfClass);
      ref = getClasses(rdfClass);
      for (j = 0, len1 = ref.length; j < len1; j++) {
        rdfClass = ref[j];
        if (rdfClasses.indexOf(rdfClass) === -1) {
          rdfClasses.push(rdfClass);
        }
      }
    }
    return rdfClasses;
  };

  getFieldHTML = function (fieldObject) {
    var fieldHTML, template;
    template = templates[fieldObject["tgforms:hasInput"]];
    fieldHTML = Mustache.render(template, fieldObject);
    if (isResource(fieldObject["sh:PropertyShape"])) {
      fieldHTML = fieldHTML.replace(labelSearch, resourceReplace);
    }
    if (fieldObject["tgforms:isRepeatable"]) {
      fieldHTML = fieldHTML.replace(labelSearch, repeatReplace);
    }
    if (fieldObject["tgforms:isMandatory"]) {
      fieldHTML = fieldHTML.replace(labelSearch, mandatoryReplace);
    }
    return fieldHTML;
  };

  getFieldObject = function (fieldName) {
    var buttonID,
      field,
      formularToLoad,
      i,
      key,
      len,
      propTriple,
      propTriples,
      propertyName,
      value;
    field = {};
    path = store.find(null, "sh:path", fieldName);

    if (path.length > 0) {
      field["sh:PropertyShape"] = abbrevURI(path[0].subject);
      propTriples = store.find(path[0].subject, null, null);
    } else {
      propTriples = store.find(fieldName, null, null);
      field["sh:PropertyShape"] = abbrevURI(propTriples[0].subject);
    }

    field["tgforms:hasOption"] = [];
    for (i = 0, len = propTriples.length; i < len; i++) {
      propTriple = propTriples[i];
      key = propTriple.predicate;
      key = abbrevURI(key);
      value = propTriple.object;
      if (util.isLiteral(value)) {
        value = util.getLiteralValue(value);
      }
      value = abbrevURI(value);

      /*if(key === "sh:datatype"){
    	  datatype=store.find(fieldName, "sh:datatype", null);
    	  if(abbrevURI(datatype[0].object) === "xsd:boolean"){
    		  field["tgforms:hasInput"] = "subFormsSHACL:checkbox";
    	  }
      }*/
      if(key === "dash:editor"){        
        var linkToRemoteDescription = store.find(field["sh:PropertyShape"],"dash:editor", null);
        var remoteAddress = store.find(linkToRemoteDescription[0].object, null, null);

        if(typeof remoteAddress[0] !== "undefined" && abbrevURI(remoteAddress[0].object) === "roger:SelectInstanceFromRemoteDatasetEditor" ){
          var remoteAPIAdress = store.find(linkToRemoteDescription[0].object, "roger:useOpenSearchDescription", null);
          field["roger:remoteAddress"] = remoteAPIAdress[0].object.replace(/"/g, "");
          field["roger:remoteSearch"] = true;
        }else{
          field["roger:remoteSearch"] = false;
        }     
      }
      if (key === "sh:class") {
        var name = store.find(field["sh:PropertyShape"], "sh:class", null);
        field["sh:class"] = abbrevURI(name[0].object);
      }
      if (key === "sh:node" || key === "sh:nodeKind") {
        field["tgforms:hasInput"] = "subFormsSHACL:button";
      }

      if (key === "sh:in") {
        var list = getList(value);
        //options = [];
        //options = value.split(",");
        field["tgforms:hasInput"] = "subFormsSHACL:dropdown";
        for (j = 0, length = list.length; j < length; j++) {
          field["tgforms:hasOption"].push(list[j]);

        }
      } else {
        field[key] = value;
      }
      if (
        key === "dash:editor" &&
        store.find(field["sh:PropertyShape"], "dash:singleLine", null).length >
          0
      ) {
        singleLine = store
          .find(field["sh:PropertyShape"], "dash:singleLine", null)[0]
          ["object"].replace("^^http://www.w3.org/2001/XMLSchema#boolean", "");

        if (singleLine === '"true"') {
          field["tgforms:hasInput"] = "subFormsSHACL:text";
        }
        if (singleLine === '"false"') {
          field["tgforms:hasInput"] = "subFormsSHACL:textarea";
        }
      }
      if (key === "sh:minCount") {
        mandatory = store.find(field["sh:PropertyShape"], "sh:minCount", null);
        mandatory = mandatory[0].object.replace(
          "^^http://www.w3.org/2001/XMLSchema#integer",
          ""
        );
        field["tgforms:isMandatory"] = "true";
      }
      if (key === "sh:maxCount") {
        repeatable = store.find(field["sh:PropertyShape"], "sh:maxCount", null);
        repeatable = repeatable[0].object.replace(
          "^^http://www.w3.org/2001/XMLSchema#integer",
          ""
        );
        if (repeatable === '"1"') {
          field["tgforms:isRepeatable"] = false;
        }
      }
      if (key === "sh:order") {
        orderNumber = store
          .find(field["sh:PropertyShape"], "sh:order", null)[0]
          .object.replace("^^http://www.w3.org/2001/XMLSchema#decimal", "");
        //console.log(orderNumber);
        field["tgforms:hasPriority"] = parseInt(
          orderNumber.replace('"', ""),
          10
        );
        //console.log(field["tgforms:hasPriority"]);
      }
    }
    if (!field["tgforms:hasInput"]) {
      field["tgforms:hasInput"] = "subFormsSHACL:text";
    }
    //field["tgforms:hasOption"] = field["tgforms:hasOption"];

    if (field["tgforms:isRepeatable"] !== false) {
      field["tgforms:isRepeatable"] = true;
    } else {
      field["tgforms:isRepeatable"] = false;
    }

    if (typeof field["sh:node"] !== "undefined") {
      var tmpButtonID = field["sh:path"];
      //console.log(field["sh:node"]);
      tmpButtonID = tmpButtonID.substring(tmpButtonID.indexOf(":")+1, tmpButtonID.length);
      buttonID = tmpButtonID;
      field["tgforms:buttonID"] = buttonID;
      propertyName = field["sh:path"];
      formularToLoad = field["sh:node"];
      propertyName = propertyName.split(":");
      //console.log(formularToLoad);
      //console.log(propertyName);
      $(document).on('click', 'div.' + propertyName[0] + '\\:' + propertyName[1] + ' button#' + buttonID, function(e) {
        var buttonUUID, fdiv, genid, genid2;
        if ($(this).attr('disabled') !== "disabled") {
          divid = generateId();
          genid = propertyName[0] + '_' + propertyName[1] + divid;
          genid2 = propertyName[0] + ':' + propertyName[1] + divid;
          $(this).parent().find('span.value').html(genid2);
          buttonUUID = generateId();
          //console.log(genid);
          //console.log(propertyName[1]);
          //console.log('<div id="' + genid + '" class="' + propertyName[1] + ' subformular collapse in "' + buttonID + '">content</div>');
          fdiv = $('<div id="' + genid + '" class="' + propertyName[1] + ' subformular"' + buttonID + '">content</div>');
          $(this).parent().append(fdiv);
         // console.log(formularToLoad);
          //console.log(fdiv);
          var classForTargetClass = store.find(formularToLoad, "sh:targetClass", null);
          //console.log(classForTargetClass);
          var subFormName = abbrevURI(classForTargetClass[0].object);
          //console.log(store.find(null, "sh:targetClass", formName))
          //console.log("SubFormName: " + subFormName);
          //console.log("fdiv: " + fdiv);
          shacl.buildForm(subFormName, fdiv);
          //shacl.fillForm(genid2, fdiv);
          $(this).attr('disabled', 'disabled');
          return $(this).next().removeAttr('disabled');
        }
      });
    }
    /*if (typeof field["sh:node"] !== "undefined") {
      var tmpButtonID = field["sh:path"];
      tmpButtonID = tmpButtonID.substring(
        tmpButtonID.indexOf(":") + 1,
        tmpButtonID.length
      );
      buttonID = tmpButtonID;
      console.log(fieldName);
      console.log(abbrevURI(fieldName));
      var blablubb = store.find("idiomshapes:E67_BirthNodeShape", "sh:targetClass",null);
      //console.log(blablubb);
      console.log(abbrevURI(blablubb[0].object));

      var tmpFieldToGetClassNameForSubFormular = store.find(abbrevURI(fieldName), "sh:node", null);
      tmpFieldToGetClassNameForSubFormular = tmpFieldToGetClassNameForSubFormular[0].object;
      console.log(abbrevURI(tmpFieldToGetClassNameForSubFormular));
      //console.log(store.find(tmpFieldToGetClassNameForSubFormular, "sh:node", null));
      //console.log(store.find(abbrevURI(tmpFieldToGetClassNameForSubFormular), "sh:node", null));
      var generatorFieldForSubFormular = store.find(abbrevURI(tmpFieldToGetClassNameForSubFormular), "sh:targetClass",null);
      if(typeof generatorFieldForSubFormular[0].object != "undefined"){
        generatorFieldForSubFormular = abbrevURI(generatorFieldForSubFormular[0].object);
        console.log(generatorFieldForSubFormular);
        field["tgforms:buttonID"] = generatorFieldForSubFormular;
      }
       //abbrevURI(blablubb[0].object) ;
      propertyName = field["sh:path"];
      formularToLoad = field["sh:node"];
      propertyName = propertyName.split(":");

      $(document).on(
        "click",
        "div " +
          propertyName[0] +
          "\\:" +
          propertyName[1] +
          " button#" +
          buttonID,
        function (e) {
          var buttonUUID, fdiv, genid, genid2;
          if ($(this).attr("disabled") !== "disabled") {
            divid = generateId();
            genid = propertyName[0] + "_" + propertyName[1] + divid;
            genid2 = propertyName[0] + ":" + propertyName[1] + divid;
            $(this).parent().find("span.value").html(genid2);
            buttonUUID = generateId();
            fdiv = $(
              '<div id="' +
                genid +
                '" class="' +
                propertyName[1] +
                ' subformular collapse in "' +
                buttonID +
                '">content</div>'
            );
            $(this)
              .parent()
              .append(
                '<button type="button" data-toggle="collapse" data-target="div#' +
                  genid +
                  '"><span class="glyphicon glyphicon-menu-down"></span></button>'
              )
              .append(fdiv);
            shacl.buildForm(formularToLoad, fdiv);  
            $(this).attr("disabled", "disabled");
            return $(this).next().removeAttr("disabled");
          }
        }
      );
    }*/
    return field;
  };

  getFormTriples = function (subject) {
    var formTriples,
      i,
      j,
      len,
      len1,
      listStart,
      rdfClass,
      rdfClasses,
      triple,
      triples,
      fieldExperiment;
    formTriples = [];
    rdfClasses = getClasses(subject);

    for (i = 0, len = rdfClasses.length; i < len; i++) {
      rdfClass = rdfClasses[i];
      triples = store.find(rdfClass, "sh:property", null);
      for (j = 0, len1 = triples.length; j < len1; j++) {
        triple = triples[j];

        fieldExperiment = store.find(triple.object, "sh:name", null);
        //if (triple.predicate === expandPrefix("rdfs:domain")) {
        formTriples.push(fieldExperiment);
        //}
      }
    }
    return formTriples;
  };

  getList = function (subject) {
    var element, firstObject, i, len, list, ref, restObject;
    list = [];
    firstObject = store.find(subject, "rdf:first", null)[0].object;
    restObject = store.find(subject, "rdf:rest", null)[0].object;
    list.push(abbrevURI(firstObject));
    if (abbrevURI(restObject) !== "rdf:nil") {
      ref = getList(restObject);
      for (i = 0, len = ref.length; i < len; i++) {
        element = ref[i];
        list.push(abbrevURI(element));
      }
    }
    return list;
  };

  getListStart = function (object) {
    var triple;
    triple = store.find(null, null, object);
    if (util.isBlank(triple[0].subject)) {
      return getListStart(triple[0].subject);
    } else {
      return triple[0];
    }
  };

  getPrefixes = function () {
    return store._prefixes;
  };

  getUnionOf = function (subject, predicate) {
    var mainObject, unionOfObject;
    mainObject = store.find(subject, predicate, null)[0].object;
    unionOfObject = store.find(mainObject, "owl:unionOf", null)[0].object;
    return unionOfObject;
  };

  isResource = function (subject) {
    var element, i, len, rangeObject, ref, result;
    if (
      store.find(subject, "sh:targetClass", null).length !== 0 ||
      store.find(subject, "sh:nodeKind", "sh:BlankNode").length !== 0 ||
      store.find(subject, "sh:nodeKind", "sh:IRI").length !== 0
    ) {
      result = true;
    } else {
      result = false;
    }

    return result;
  };

  prefixCall = function (prefix, uri) {
    return store.addPrefix(prefix, uri);
  };

  repeatField = function () {
    var $this, fieldHTML, fieldName, focusCall;
    $this = $(this);
    fieldName = $this.closest("div.form-group").attr("data-tgforms-name");
    fieldHTML = getFieldHTML(getFieldObject(fieldName));
    fieldHTML = fieldHTML.replace(labelSearch, deleteReplace);
    $this.closest("div.form-group").after(fieldHTML);
    $("span.repeat").unbind("click").click(repeatField);
    $this
      .closest("div.form-group")
      .next()
      .find("span.deleteField")
      .click(function () {
        return $(this).closest("div.form-group").remove();
      });
    focusCall = function () {
      return $this.closest("div.form-group").next().find("input").focus();
    };
    return setTimeout(focusCall, 25);
  };

  sortFields = function (a, b) {
    if (a["tgforms:hasPriority"] < b["tgforms:hasPriority"]) {
      return -1;
    } else {
      return 1;
    }
  };

  subFormsSHACL.prototype.abbrevURI = function (string) {
    return abbrevURI(string);
  };

  subFormsSHACL.prototype.addTurtle = function (turtle, addCall) {
    var tripleCall;
    tripleCall = function (error, triple, prefixes) {
      if (triple) {
        return store.addTriple(triple);
      } else {
        return addCall();
      }
    };
    return parser.parse(turtle, tripleCall, prefixCall);
  };

  subFormsSHACL.prototype.getFormularClasses = function () {
    var shaclFormNodes = store.find(null, "roger:formNode", "\"true\"^^http://www.w3.org/2001/XMLSchema#boolean");
    
    if ($("button.classSelection").length === 0) {
      for (j = 0, length = shaclFormNodes.length; j < length; j++) {
        var classLabel = store.find(
          shaclFormNodes[j]["subject"],
          "rdfs:label",
          null
        );
        var realFormType = store.find(shaclFormNodes[j]["subject"], "sh:targetClass", null);
        var abFormType = abbrevURI(realFormType[0].object);
        //TODO: redeem the replace for Shape. Choose correct by generating form
        var className = abbrevURI(shaclFormNodes[j]["subject"])
          .replace("Shape", "")
          .replace("shapes", "");
        $("div.modal-body div.container-fluid div.classSelection.row").append(
          '<div class="col-md-6 .ms-auto"><button class="btn btn-primary classSelection" type="submit" name="' +
            abFormType +
            '">' +
            classLabel[0]["object"].replace(/"/g, "") +
            "</button></div>"
        );
      }
    }

    return shaclFormNodes;
  };
  //TODO: could getFormularClasses be used?
  /*subFormsSHACL.prototype.getFormularClassesForOpeningObjects = function(){
	  var shaclFormNodes = store.find(null,"tgforms:FormNode", null);

	  if($("button.listClassObjects").length === 0){
	      for(j = 0, length = shaclFormNodes.length; j < length; j++){
	    	  var classLabel = store.find(shaclFormNodes[j]["subject"],"rdfs:label", null)
	    	  var className = abbrevURI(shaclFormNodes[j]["subject"]).replace("shapes", "").replace("Shape", "");
	          $('div#openDataObject div.modal-body div.container-fluid div#objectClassRow0').append('<div class="col-md-6 .ms-auto"><button class="btn btn-primary listClassObjects" type="submit" name="' + className + '">' + classLabel[0]["object"] + '</button></div><div id=\"objectList-' + className.replace("\:", "_") + '\"></div>')
	      }
  	  }

	  return shaclFormNodes;
  }*/

  subFormsSHACL.prototype.buildForm = function (formName, selector) {
    var classHelpText,
      classHelpTexts,
      fieldObject,
      form,
      formHTML,
      formTriple,
      formularDisplayName,
      formularDisplayName2,
      formularNames,
      i,
      j,
      k,
      l,
      len,
      len1,
      len2,
      len3,
      ref;

    form = [];
    console.log(formName);
    if(formName.startsWith("http")){
      console.log("starts magic");
      formName = abbrevURI(formName);
      console.log("magic done");
      console.log(formName);
    }
    tmpFormNameToGetLabel = store.find(null, "sh:targetClass", formName);
    console.log(tmpFormNameToGetLabel);
    tmpFormNameToGetLabel = abbrevURI(tmpFormNameToGetLabel[0].subject);

    var rdfsClassLabel = (formularNames = store.find(
      tmpFormNameToGetLabel,
      "rdfs:label",
      null
    ));
    rdfsClassLabel = rdfsClassLabel[0].object.replace(/"/g, "");

    classHelpTexts = store.find(formName, "rdfs:comment", null);
    for (j = 0, len1 = classHelpTexts.length; j < len1; j++) {
      classHelpText = classHelpTexts[j];
      helptext = classHelpText.object;
    }

    formHTML =
      '<form role="form" class="tgForms" name="' +
      formName +
      '"> <fieldset class="title"> <legend class="title"> <span class="legendSpan" title=' +
      helptext +
      ">" +
      rdfsClassLabel +
      "</span> </legend>";
    ref = getFormTriples(tmpFormNameToGetLabel);

    for (k = 0, len2 = ref.length; k < len2; k++) {
      formTriple = ref[k][0];
      form.push(getFieldObject(formTriple.subject));
    }
    form = form.sort(sortFields);
    for (l = 0, len3 = form.length; l < len3; l++) {
      fieldObject = form[l];
      formHTML += getFieldHTML(fieldObject);
    }
    formHTML += "</fieldset></form>";
    $(selector).html(formHTML);
    $("div.subformular").prev().removeAttr("disabled");
    $("div.subformular").prev().prev().attr("disabled", "disabled");
    $("button.removeContent").each(function () {
      if ($(this).prev().prev().text().length > 0) {
        $(this).removeAttr("disabled");
        return $(this).prev().attr("disabled", "disabled");
      }
    });
    $("span.repeat").unbind("click").click(repeatField);
    $("form.tgForms").on("click", "li a.formDDItem", function () {
      return $(this)
        .closest("div.dropdown")
        .find("span.value")
        .text($(this).text().replace(/"/g, ""));
    });
    return $(".dropdown-toggle").click(function (e) {
      return e.preventDefault();
    });
  };

  subFormsSHACL.prototype.fillForm = function (subject, selector) {
    var $this,
      escapedName,
      fieldHTML,
      fieldName,
      i,
      len,
      object,
      predicate,
      triple,
      triples;
    triples = store.find(subject, null, null);
    for (i = 0, len = triples.length; i < len; i++) {
      triple = triples[i];
      predicate = triple.predicate;
      fieldName = abbrevURI(predicate);

      object = triple.object;

      if (util.isLiteral(object)) {
        object = util.getLiteralValue(object);
      }
      object = abbrevURI(object);

      escapedName = fieldName.replace(":", "\\:");
      $this = $(selector + " div." + escapedName).last();
      if ($this.find("input").attr("type") === "checkbox") {
        if (object === "true") {
          $this.find("input").prop("checked", true);
        }
      }
      if ($this.find("input").attr("type") === "text") {
        if ($this.find("input").val()) {
          fieldHTML = getFieldHTML(getFieldObject(fieldName));
          fieldHTML = fieldHTML.replace(labelSearch, deleteReplace);
          $this.closest("div.form-group").after(fieldHTML);
          $this.closest("div.form-group").next().find("input").val(object);
        } else {
          $this.find("input").val(object);
        }
      }
      if ($this.find("span.value")) {
        if ($this.find("span.value").text()) {
          fieldHTML = getFieldHTML(getFieldObject(fieldName));
          fieldHTML = fieldHTML.replace(labelSearch, deleteReplace);
          $this.closest("div.form-group").after(fieldHTML);
          $this
            .closest("div.form-group")
            .next()
            .find("span.value")
            .text(object);
        } else {
          $this.find("span.value").text(object);
        }
      }
      if ($this.find("textarea")) {
        if ($this.find("textarea").val()) {
          fieldHTML = getFieldHTML(getFieldObject(fieldName));
          fieldHTML = fieldHTML.replace(labelSearch, deleteReplace);
          $this.closest("div.form-group").after(fieldHTML);
          $this.closest("div.form-group").next().find("textarea").val(object);
        } else {
          $this.find("textarea").val(object);
        }
      }
    }
    $("span.repeat").unbind("click").click(repeatField);
    return $("span.delete")
      .unbind("click")
      .click(function () {
        return $(this).closest("div.form-group").remove();
      });
  };

  subFormsSHACL.prototype.getFieldHTML = function (fieldObject) {
    return getFieldHTML(fieldObject);
  };

  subFormsSHACL.prototype.getFieldObject = function (fieldName) {
    return getFieldObject(fieldName);
  };

  subFormsSHACL.prototype.getFormField = function (fieldName) {
    return getFieldObject(fieldName);
  };

  subFormsSHACL.prototype.getInput = function (subject, type, selector) {
    var jsonLD;
    jsonLD = {
      "@context": getPrefixes(),
      "@id": subject,
      "@type": type,
    };
    $(selector + " input")
      .filter(function () {
        return (
          $(this).closest("fieldset").closest("div").attr("id") ===
          $(selector).attr("id")
        );
      })
      .each(function () {
        var $this;
        $this = $(this);
        return (jsonLD = addToJSONLD(jsonLD, $this));
      });
    $(selector + " span.value")
      .filter(function () {
        return (
          $(this).closest("fieldset").closest("div").attr("id") ===
          $(selector).attr("id")
        );
      })
      .each(function () {
        var $this;
        $this = $(this);
        return (jsonLD = addToJSONLD(jsonLD, $this));
      });
    $(selector + " textarea")
      .filter(function () {
        return (
          $(this).closest("fieldset").closest("div").attr("id") ===
          $(selector).attr("id")
        );
      })
      .each(function () {
        var $this;
        $this = $(this);
        return (jsonLD = addToJSONLD(jsonLD, $this));
      });
    return jsonLD;
  };

  subFormsSHACL.prototype.getPrefixes = function () {
    return getPrefixes();
  };

  subFormsSHACL.prototype.getStore = function () {
    return store;
  };

  subFormsSHACL.prototype.getType = function (subject) {
    var type;
    type = store.find(subject, "rdf:type", null)[0].object;
    if (util.isLiteral(type)) {
      type = util.getLiteralValue(type);
    }
    return abbrevURI(type);
  };

  subFormsSHACL.prototype.renderField = function (fieldObject) {
    return getFieldHTML(fieldObject);
  };

  generateId = function () {
    var chars, result, today;
    chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    today = new Date();
    result = today.valueOf().toString(16);
    result += chars.substr(Math.floor(Math.random() * chars.length), 1);
    result += chars.substr(Math.floor(Math.random() * chars.length), 1);
    return result;
  };

  getDivId = function () {
    return divid;
  };

  clearForm = function () {
    var divID;
    return (divID = $(this).attr("id"));
  };

  $(document).on("click", "button.clearForm", function (e) {
    var divID;
    e.preventDefault();
    divID = $(this).attr("id");
    $("div#" + divID + " input").val("");
    $("div#" + divID + " textarea").val("");
    return $(
      "div#" +
        divID +
        ' button[class="btn btn-secondary dropdown-toggle"] span[class="value"]'
    ).text("");
  });

  $(document).on("click", "button#removeContent", function (e) {
    var divID;
    e.preventDefault();
    divID = $(this).parent().parent().attr("data-tgforms-name");
    divID = divID.replace(":", "\\:");
    $(this).prev().prev().text("");
    $(this).attr("disabled", "disabled");
    return $(this).prev().removeAttr("disabled");
  });

  $(document).on("click", "button.removeFormular", function (e) {
    var divID, divID2, id, string;
    e.preventDefault();
    id = $(this).attr("class").split(" ")[2];
    divID = $(this).parent().parent().attr("data-tgforms-name");
    divID = divID.replace(":", "\\:");
    divID2 = $(this).parent().children(":first-child").text();
    divID2 = divID2.replace(":", "_");
    $("div#" + divID2).remove();
    $(this).parent().children(":first-child").text("");
    $("button[data-target='div#" + divID2 + "']").remove();
    $(this).attr("disabled", "disabled");
    $(this).prev().removeAttr("disabled");
    return (string = $(this).prev().text());
  });

  return subFormsSHACL;
})();


/*async function checkMandatoryField(){
  $("[required]").each(function() {
    if($(this).val().length === 0){
      $(this).attr("style", "border:2px solid #ff0000");
      $(this).attr("beforeSave", true);
    }else{
      $(this).removeAttr("style");
      $(this).removeAttr("beforeSave");
    }
  });
  if($("[beforeSave]").length>0){
    return false;
  }else{
    return true;
  }
}*/

async function checkMandatory(divContentId) {
  var mandatoryInputFilled;
  mandatoryInputFilled = true;
  $("div#" + divContentId + " input[required]").each(function () {
    if ($(this).val().length === 0) {
      return (mandatoryInputFilled = false);
    } else {
      $(this).removeAttr("style");
      return (mandatoryInputFilled = true);
    }
  });
  if (mandatoryInputFilled === false) {
    alert("Fill in the required fields");
    return $("input[required]").each(function () {
      if ($(this).val().length === 0) {
        return $(this).attr("style", "border:1px solid #ff0000");
      }
    });
  } else {
    return (mandatoryInputFilled = true);
  }
};

async function checkMandatoryButton(divContentId) {
  var mandatoryButtonFilled;
  mandatoryButtonFilled = true;
  $("div#" + divContentId + " button.formularOp[required], button.searchOp[required]").each(function () {
    if ($(this).prev().text().length === 0) {
      return (mandatoryButtonFilled = false);
    } else {
      $(this).removeAttr("style");
      return (mandatoryButtonFilled = true);
    }
  });
  if (mandatoryButtonFilled === false) {
    alert("Fill in the required fields");
    return $("div#" + divContentId + " button.formularOp[required], button.searchOp[required]").each(
      function () {
        if ($(this).prev().text().length === 0) {
          return $(this).attr("style", "border:1px solid #ff0000");
        }
      }
    );
  } else {
    return (mandatoryButtonFilled = true);
  }
};

async function checkMandatoryDropdown(divContentId) {
  var mandatoryDropdownFilled;
  mandatoryDropdownFilled = true;
  $("div#" + divContentId + " button.dropdown-toggle[required]").each(function () {
    if ($(this).children().text().length === 0) {
      return (mandatoryDropdownFilled = false);
    } else {
      $(this).removeAttr("style");
      return (mandatoryDropdownFilled = true);
    }
  });
  if (mandatoryDropdownFilled === false) {
    alert("Fill in the required fields");
    return $("div#" + divContentId + " button.dropdown-toggle[required]").each(function () {
      if ($(this).children().text().length === 0) {
        return $(this).attr("style", "border:1px solid #ff0000");
      }
    });
  } else {
    return (mandatoryDropdownFilled = true);
  }
};

/*$(document).on(
  "ready click",
  "li a.formDDItem",
  function () {
    var valueToSetForDropdown = $(this).text();
    valueToSetForDropdown = valueToSetForDropdown.replace(/"/g, "");
    $(this).parent().parent().prev().text(valueToSetForDropdown);
    $(this)
  });*/
