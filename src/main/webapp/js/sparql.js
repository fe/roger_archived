//const sparqlEndpoint = "http://fuseki:3030/roger";
const sparqlQueryBegin = "SELECT * WHERE {GRAPH ?graph {";
const sparqlQueryEnd = "}} LIMIT ";
//var shacl = new subFormsSHACL();
const resultsPerPage = "3";
/*function searchForObjectInTriplestore (prefixes, rdfTypeForObjectSearch){
  //var searchValue=$("input#objectSearch-input").val();
  query= prefixes +
    "SELECT ?graph ?object ?type WHERE {GRAPH ?graph {?subject dcterms:title ?object. ?subject rdf:type " + rdfTypeForObjectSearch + ". }}";
}*/

$(document).on("ready click", "button#generalObjectSearch", function () {
  var query =
    "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>prefix idiom: <http://idiom-projekt.de/schema/> prefix shacl: <http://idiom-projekt.de/idiomshapes#>" +
    "SELECT ?graph ?object ?type WHERE {GRAPH ?graph {?subject dcterms:title ?object. ?subject rdf:type ?type}}";
  query = encodeURIComponent(query);
  var request = { query: query };
  $.ajax({
    type: "GET",
    url: "db/trip?query=" + query,
    cache: false,
    dataType: "json",
    data: request,
  }).done(function (data) {
    var results = data["results"]["bindings"];
    for (var j = 0, len = results.length; j < len; j++) {
      var result = results[j];
      var uri = result["graph"]["value"];
      var object = result["object"]["value"];
      var type = result["type"]["value"];
      $("div#objectSearch-results").append(
        '<div class="radio"><input type="radio" class="objectChoice" name="objectMetadata-radios" value="' +
          uri +
          '"><strong>' +
          uri +
          " || " +
          object +
          "</strong> Type: " +
          type +
          " </div>"
      );
    }
  });
});

function listObjectsOfClass(prefixes, rdfTypeForObjectSearch) {
  var query =
    prefixes +
    " SELECT * WHERE {GRAPH ?graph {?subject rdf:type " +
    rdfTypeForObjectSearch +
    ". ?subject dcterms:title ?title.}}";
  query = encodeURIComponent(query);
  var request = { query: query };
  $.ajax({
    type: "GET",
    url: "db/trip?query=" + query,
    cache: false,
    dataType: "json",
    data: request,
  }).done(function (data) {
    var results = data["results"]["bindings"];
    for (var j = 0, len = results.length; j < len; j++) {
      var result = results[j];
      var uri = result["graph"]["value"];
      var title = result["title"]["value"];
      $("div#objectList-" + rdfTypeForObjectSearch.replace(":", "_")).append(
        '<div class="radio"><input type="radio" class="objectChoice" name="objectMetadata-radios" value="' +
          uri +
          '"><strong class="subFormObjectChoice">' +
          title +
          " (" +
          uri +
          ")</div>"
      );
    }
  });
}

function filterObjectList(rdfTypeForObjectSearch) {
  var prefixes = shacl.getPrefixes();
  var prefixesForTripleStore = "";
  for (var prefix in prefixes) {
    prefixesForTripleStore =
      prefixesForTripleStore +
      "PREFIX " +
      prefix +
      ": <" +
      prefixes[prefix] +
      ">";
  }
  
  var query = prefixesForTripleStore + buildQuery(rdfTypeForObjectSearch);
  query = encodeURIComponent(query);
  var request = { query: query };
  $.ajax({
    type: "GET",
    url: "db/trip?query=" + query,
    cache: false,
    dataType: "json",
    data: request,
  }).done(function (data) {    
    var results = data["results"]["bindings"];
    checkNextButton(results.length, resultsPerPage);
    for (var j = 0, len = results.length; j < len; j++) {
      var result = results[j];
      var objectMetadata = "";
      for (var res in result) {
        if (res !== "subject") {
          objectMetadata =
            objectMetadata +
            '<strong class="subFormObjectChoice">' +
            res +
            ": </strong>" +
            result[res]["value"];
        }
      }
      $("div#triplestoreResultList").append(
        '<div class="radio"><input type="radio" class="objectChoice" name="objectMetadata-radios" value="' +
          result["graph"]["value"] +
          '">' +
          objectMetadata +
          "</div>"
      );
      //$("div#triplestoreResultList").append("<div class=\"radio\"><input type=\"radio\" class=\"objectChoice\" name=\"objectMetadata-radios\" value=\"" + uri + "\"><strong class=\"subFormObjectChoice\">" + "title" + " (" + uri + ")</div>");
    }
  });
  $("button#nextTripleStoreResults").unbind("click");
  $("button#nextTripleStoreResults").click(function (e) {
    $("div#triplestoreResultList").empty();
    var tripStoreOffset = $("input#tripleStoreOffset").val();
    $("input#tripleStoreOffset").val(parseInt(parseInt(tripStoreOffset) + resultsPerPage));
    query = decodeURIComponent(query);
    query = query.replace("OFFSET " + tripStoreOffset, "");
    return processQuery(query + "OFFSET " + $("input#tripleStoreOffset").val());
  });
  $("button#prevTripleStoreResults").unbind("click");
  $("button#prevTripleStoreResults").click(function (e) {
    $("div#triplestoreResultList").empty();
    var tripStoreOffset = $("input#tripleStoreOffset").val();
    $("input#tripleStoreOffset").val(parseInt(parseInt(tripStoreOffset) - resultsPerPage));
    query = decodeURIComponent(query);
    query = query.replace("OFFSET " + tripStoreOffset, "");
    return processQuery(query + "OFFSET " + $("input#tripleStoreOffset").val());
  });
}

function setPredicateList(prefixes, subject) {
  const shapeClass = shacl.getStore().find(null, "sh:targetClass", subject);
  const storeSubjectOfShapeClass = shacl.abbrevURI(shapeClass[0].subject);
  const predicateList = shacl
    .getStore()
    .find(storeSubjectOfShapeClass, "sh:property", null);
  var predicates = "?subject rdf:type " + subject + ".\n";
  for (var i = 0; i < predicateList.length; i++) {
    var object = shacl
      .getStore()
      .find(shacl.abbrevURI(predicateList[i].object), "sh:path", null);
    object = shacl.abbrevURI(object[0].object);

    predicateList[i] =
      "OPTIONAL{?subject " +
      object +
      " ?" +
      object.substring(object.indexOf(":") + 1, object.length) +
      "}.\n";
    predicates = predicates + " " + predicateList[i];
  }
  processQuery(
    prefixes + "\n " + sparqlQueryBegin + "\n" + predicates + sparqlQueryEnd + " " + resultsPerPage,
    subject
  );
}

function buildQuery(subject) {
  const shapeClass = shacl.getStore().find(null, "sh:targetClass", subject);
  const storeSubjectOfShapeClass = shacl.abbrevURI(shapeClass[0].subject);
  const predicateList = shacl
    .getStore()
    .find(storeSubjectOfShapeClass, "sh:property", null);
  var predicates = "?subject rdf:type " + subject + " ";
  for (var i = 0; i < predicateList.length; i++) {
    var object = shacl
      .getStore()
      .find(shacl.abbrevURI(predicateList[i].object), "sh:path", null);
    object = shacl.abbrevURI(object[0].object);
    var fieldName = shacl
      .getStore()
      .find(shacl.abbrevURI(predicateList[i].object), "sh:name", null);
    fieldName = fieldName[0].object;
    predicateList[i] =
      "OPTIONAL{?subject " +
      object +
      " ?" +
      fieldName
        .replace(/"/g, "")
        .replace(/ /g, "_")
        .replace("(", "")
        .replace(")", "")
        .replace(".", "") +
      "}.";
    predicates = predicates + " " + predicateList[i];
  }
  var filterList = "";
  $("select.form-select").each(function (index) {
    
    var filterFieldName = $(
      "div#filter" +
        index +
        " select.form-select option[value='" +
        $(this).val() +
        "']"
    ).text();
    var keyword = $("div.filterSettings#filter" + index + " input").val();
    if(keyword.length>0){
      filterList +=
        "FILTER (regex (str(?" +
        filterFieldName
          .replace(/\./g, "")
          .replace(/ /g, "_")
          .replace("(", "")
          .replace(")", "")
          .replace(".", "") +
        '), "' +
        keyword +
        '", "i"))';
    }
  });

  console.log(sparqlQueryBegin + predicates + filterList + sparqlQueryEnd + " " + resultsPerPage);

  return sparqlQueryBegin + predicates + filterList + sparqlQueryEnd + " " + resultsPerPage;
}

function buildFilterDropdown(subject) {
  var filterCount = $("div.filterSettings").length;
  if($("div.queryFilterContainer").length === 0){
    $("div#triplestoreResult div.modal-body form").before("<div class=\"queryFilterContainer\" filterName=" + subject + "></div>");
  }
  var select =
    '<div class=filterSettings id="filter' +
    filterCount +
    '"><div class="queryFilter" id="filter' +
    filterCount +
    '">' +
    '<select class="form-select" aria-label="Default select example">' +
    "</select>" +
    "</div>" +
    '<div class="queryFilter">' +
    '<input type="text" class="form-control keyword" placeholder="Keyword" style="width:200px;  margin-left:100px">' +
    "</div>" +
    '<div class="queryFilter">' +
    '<span class="addFilter fa fa-plus icon-plus" aria-hidden="true"></span>' +
    
    "</div>";  
  $("div.queryFilterContainer").append(select);
  if($("div.filterSettings").length >1){
    $("div#filter" + filterCount + " span.addFilter").after('<span class="removeFilter fa fa-minus icon-minus" aria-hidden="true"></span>');
  }
   
  const shapeClass = shacl.getStore().find(null, "sh:targetClass", subject);
  const storeSubjectOfShapeClass = shacl.abbrevURI(shapeClass[0].subject);
  const predicateList = shacl
    .getStore()
    .find(storeSubjectOfShapeClass, "sh:property", null);

  for (var i = 0; i < predicateList.length; i++) {
    var object = shacl
      .getStore()
      .find(shacl.abbrevURI(predicateList[i].object), "sh:path", null);
    object = shacl.abbrevURI(object[0].object);
    var fieldName = shacl
      .getStore()
      .find(shacl.abbrevURI(predicateList[i].object), "sh:name", null);
    fieldName = fieldName[0].object;
    $("div#filter" + filterCount + " select.form-select").append(
      '<option value="' +
        i +
        '" name="' +
        object +
        '">' +
        fieldName.replace(/"/g, "") +
        "</option>"
    );
  }
}

$(document).on("ready click", "span.removeFilter", function () {
  //var oblectClassForFilter = 
  $(this).parent().parent().remove();
  //buildFilterDropdown(oblectClassForFilter);
  $(this).unbind("click");
});

$(document).on("ready click", "span.addFilter", function () {
  var oblectClassForFilter = $(this).parent().parent().parent().attr("filterName");
  buildFilterDropdown(oblectClassForFilter);
  $(this).unbind("click");
});

function processQuery(query, objectClassType) {
  checkPrevButton();
  query = encodeURIComponent(query);
  var request = { query: query };

  $.ajax({
    type: "GET",
    url: "db/trip?query=" + query,
    cache: false,
    dataType: "json",
    data: request,
  }).done(function (data) {
    var results;
    results = data["results"]["bindings"];
    checkNextButton(results.length, resultsPerPage);
    for (var j = 0; j < results.length; j++) {
      var result = results[j];
      var objectMetadata = "";
      for (var res in result) {
        if (res !== "subject") {
          objectMetadata =
            objectMetadata +
            '<strong class="subFormObjectChoice">' +
            res +
            ": </strong>" +
            result[res]["value"];
        }
      }
      $("div#triplestoreResultList").append(
        '<div class="radio"><input type="radio" class="objectChoice" name="objectMetadata-radios" value="' +
          result["graph"]["value"] +
          '">' +
          objectMetadata +
          "</div>"
      );
    }
  });

  $("button#prevTripleStoreResults").unbind("click");
  $("button#prevTripleStoreResults").click(function (e) {
    e.preventDefault();
    
    $("div#triplestoreResultList").empty();
    var tripStoreOffset = $("input#tripleStoreOffset").val();
    $("input#tripleStoreOffset").val(parseInt(parseInt(tripStoreOffset) - resultsPerPage));
    query = decodeURIComponent(query);
    query = query.replace("OFFSET " + tripStoreOffset, "");
    return processQuery(query + "OFFSET " + $("input#tripleStoreOffset").val());
  });
  $("button#nextTripleStoreResults").unbind("click");
  $("button#nextTripleStoreResults").click(function (e) {
    e.preventDefault();
    $("div#triplestoreResultList").empty();
    var tripStoreOffset = $("input#tripleStoreOffset").val();
    $("input#tripleStoreOffset").val(parseInt(parseInt(tripStoreOffset) + parseInt(resultsPerPage)));
    query = decodeURIComponent(query);
    query = query.replace("OFFSET " + tripStoreOffset, "");
    return processQuery(query + "OFFSET " + $("input#tripleStoreOffset").val());
  });
}

$(document).on("ready click", "button#filterSearch", function () {
  var classTypeToFilter = $(this).attr("name");
  $("button#filterSearch").off();
  $("div#triplestoreResultList").empty();  
  filterObjectList(classTypeToFilter);
});

function checkPrevButton() {
  if ($("input#tripleStoreOffset").val() == 0) {
    $("button#prevTripleStoreResults").attr("disabled", "true");
  } else if ($("input#tripleStoreOffset").val() != 0) {
    $("button#prevTripleStoreResults").removeAttr("disabled");
  }
}

function checkNextButton(resultAmount, offset) {
  if (resultAmount < offset) {
    $("button#nextTripleStoreResults").attr("disabled", "true");
  } else if (resultAmount == offset) {
    $("button#nextTripleStoreResults").removeAttr("disabled");
  }
}
