async function remoteQuery(openSearchFileName){

  $(document).on("ready click", "span#performRemoteSearch", function () {
    keyword = $("input#objectSearch-keyword").val();
    if(openSearchFileName === "opensearch_GND_Personen.osxml"){
      processGNDQuery(keyword);
    }
  });
}

async function processGNDQuery(keyword){
  await fetch("opensearch_GND_Personen.osxml", {
    method: "GET",
  }).then(response => response.text())
    .then(data => {
      const parser = new DOMParser();
      const xml = parser.parseFromString(data, "application/xml");
      var urlTag = xml.getElementsByTagName("Url")[0];
      var url = urlTag.getAttribute("template");
      gndRequest(url.replace("{searchTerms}", keyword));
  });
}

async function gndRequest(url){
  await fetch(url, {method: "GET"})
  .then(response => response.text())
  .then(data => {
    const parser = new DOMParser();
    const xml = parser.parseFromString(data, "application/xml");
    var persons = xml.getElementsByTagName("person");
    for(var i=0; i <persons.length; i++){
      var singlePersonPndID = persons[i].getAttribute("id");
      var singlePersonName = persons[i].getElementsByTagName("match_name")[0].innerHTML;
      var singlePersonPrefName = persons[i].getElementsByTagName("preferred_name")[0].innerHTML;
      var singlePersonInfo = persons[i].getElementsByTagName("info")[0].innerHTML;
      var singlePersonLink = persons[i].getElementsByTagName("link")[0].getAttribute("target");
      console.log(singlePersonLink);
      
      $("div#triplestoreResultList").append(
        '<div class="radio"><input type="radio" class="objectChoice" name="objectMetadata-radios" value="' +
        singlePersonLink +
          '"><strong>' +
          singlePersonName +
          " || " +
          singlePersonPrefName +
          "</strong> Type: " +
          singlePersonPndID +
          " </div>"
      );
    }    
  });
}

$(document).on("ready click", "button#gndQuery", function () {
  processGNDQuery();
});
