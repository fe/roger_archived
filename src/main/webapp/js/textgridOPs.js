var tgURI = "";
var textgridTitle = "";

/**
 * Generate a TextGrid-URI and store it in a specific project and with a given title.
 *
 * tgSID : session ID for the access control of the project
 */
function createNewTextGridObject(tgPID) {
  var url =
    "db/tgOp/createTextGrid/" +    
    getSidFromURLParam() +
    "?tgPID=" +
    tgPID +
    "&title=UNTITLED";
  url = url.replace("NaN", "?tgPID=");
  return $.ajax({
    type: "GET",
    url: url,
    mimeType: "text/plain",
  }).done(function (data) {
    $("div.savingstatus").text("Storing object in TextGrid-Repository");
    data = JSON.parse(data);
    tgURI = data["@id"];
    setTGUri(tgURI);
    return data["@id"];
  });
}

function setTGUri(identifier){
  tgURI = identifier;
}

function getTGUri(){
  return tgURI;
}

/**
 * Saving the object in the TextGrid-repository and the specified URI
 * and take the contentTabID for getting the correct title
 *
 *  tgURI : identifier of the object in the textgrid repository
 *  contentTabID: ID of the tab which contains the specific textgridObject
 */

function saveToTextGrid(tgURI, contentTabID, data) {
  $("div.textgridTitle")
    .after()
    .append(
      '<div class="alert savingTextGridObject alert-info" role="alert"><div class="spinner-border" role="status"><span class="sr-only"></span></div></div>'
    );
  //$('div.savingTextGridObject').append("<div class=\"savingstatus\">Generating TextGrid-URI</div>");

  /**
   * The entry of a title for the TextGrid-Object is necessary.
   * Check if title is provided in the corresponding input field
   */

  if ($("div#" + contentTabID + " input#textGridTitle").val().length === 0) {
    alert("Provide Title for TextGrid Object");
    return $("div#content" + contentTabID + " input#textGridTitle").attr(
      "style",
      "border:1px solid #ff0000"
    );
  }

  return $.ajax({
    type: "POST",
    url:
      "db/tgOp/saveToTextGrid/" +
      tgURI +
      "?tgSID=" + getSidFromURLParam() +
      "&title=" +
      $("div#" + contentTabID + " input#textGridTitle").val(),
    contentType: "application/ld+json",
    data: data,
    cache: false,
  }).done(function () {
    /**
     * After saving replacing the spinner for saving process with the icon for saving function
     */
    $('li[name="' + contentTabID + '"] div.spinner-border').replaceWith(
      '<span class="objectFunctions saveobject resource fa fa-save icon-save-file" aria-hidden="true">' +
        "</span>"
    );
    $("div.saveInfo").append(
      '<div class="alert alert-info savedObject" role="alert">Object was saved with the URI: <strong>' +
        tgURI +
        "</strong> in the TextGrid-Repository" +
        "</div>"
    );

    $("div.savingTextGridObject").remove();
  });
}



/**
 * Sends a request to TextGrid-crud API to get the title of an object specified by its URI.
 *
 * tgURI: identifier of the object in TextGrid-Repository
 */

function getTitleOfTextGridObject(tgURI) {
  return $.ajax({
    type: "GET",
    url:
      "db/tgOp/titleOfTextGridObject?tgSID=" +
      getSidFromURLParam() +
      "&tgURI=" +
      tgURI,
    mimeType: "text/plain",
  }).done(function (data) {
    setTextgridTitle(data);
    return data;
  });
}

/**
 * CLICK EVENTS
 */

/*
 * Click event for choice of the TextGrid-Project ID.
 * The chosen ID will be setted as text for the dropdown menu
 *
 * $this contains the clicked item in the dropdown menu
 */

$(document).on("ready click", "a.dropdown-item", function () {
  $("button#textGridProjectChoice").text($(this).text());
});

/**
 * GETTER and SETTER
 */

function  setTextgridTitle (val) {
  return (textgridTitle = val);
};

function setTextGridStorage (val) {
  return (textGridStorage = val);
};

function getSidFromURLParam() {
  const sidParam = window.location.search;
  const urlParams = new URLSearchParams(sidParam);
  const sessionId = urlParams.get("sid");

  return sessionId;
};
