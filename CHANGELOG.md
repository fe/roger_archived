# 1.0.1-SNAPSHOT

- Increase version to 1.0.1-SNAPSHOT
- Format code
- Add jaxws-api v2.3.1 for javax.xml.ws.Holder
- Add licence headers
- Add class and method headers
- Put POM file version numbers in variables
- Remove unused compiler source and target versions from POM file
